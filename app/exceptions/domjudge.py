from app.exceptions.base import ResourceNotFoundException, BusinessRuleException


class ProblemTextNotFoundException(ResourceNotFoundException):
    pass


class ContestNotActiveException(BusinessRuleException):
    pass
