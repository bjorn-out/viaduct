import logging
from celery import states

from app import app, worker
from app.service import datanose_service, education_service, course_service
from app.exceptions.base import (
    ResourceNotFoundException,
    DuplicateResourceException,
    BusinessRuleException,
    ValidationException,
)

_logger = logging.getLogger(__name__)


@worker.task(bind=True)
def sync_datanose_courses(self) -> None:
    """Synchronizes all courses that are linked to a known Datanose education."""
    with app.app_context():
        year = datanose_service.get_current_academic_year()
        updated_ids = set()

        # Only update all Information Sciences related educations.
        educations = education_service.get_all_via_educations()

        # Update all live courses from Datanose.
        for i, education in enumerate(educations):
            _logger.debug(
                "[%d/%d] Updating education %s's courses",
                i,
                len(educations) + 1,
                education.en_name,
            )
            self.update_state(
                state=states.STARTED, meta={"count": len(educations) + 1, "current": i}
            )
            courses = datanose_service._execute_get_request(
                f"/{year}/Faculty/FNWI/Programmes/{education.datanose_code}/Courses"
            )

            for course in courses:
                # Skip all courses without Datanose code.
                if not course["CatalogNumber"]:
                    continue

                # Parse the course into our data format.
                course = datanose_service._parse_course_programmes(course)

                # Create course if it is not yet in the database.
                try:
                    db_course = course_service.get_course(
                        datanose_code=course["CatalogNumber"]
                    )
                    course_service.update_education_course_links(education, db_course)
                except ResourceNotFoundException:
                    try:
                        db_course = course_service.add_course_using_datanose(
                            course["CatalogNumber"]
                        )
                    except (
                        DuplicateResourceException
                        or ValidationException
                        or BusinessRuleException
                    ):
                        continue

                # Add tuples for the main education and all bounded programmes.
                updated_ids.add((education.id, db_course.id))

                if course["Programmes"]:
                    for programme in course["Programmes"]:
                        try:
                            education = (
                                education_service.get_education_by_datanose_code(
                                    programme["ProgrammeCode"]
                                )
                            )
                            if education.is_via_programme:
                                updated_ids.add((education.id, db_course.id))
                        except ResourceNotFoundException:
                            continue

        # Remove year and periods for discontinued courses.
        _logger.debug(
            "[%d/%d] Removing year and periods for discontinued courses",
            len(educations),
            len(educations) + 1,
        )
        self.update_state(
            state=states.STARTED,
            meta={"count": len(educations) + 1, "current": len(educations)},
        )
        education_ids = [e.id for e in educations]
        education_course_to_update = (
            course_service.get_education_course_by_exclusion_ids(
                list(updated_ids), education_ids
            )
        )

        for education_id, course_id in education_course_to_update:
            course_service.update_education_course_year_periods(
                education_id, course_id, None, None
            )
