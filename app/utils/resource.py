import re

from app import app


def get_all_routes():
    base_url_regex = r"^(/[^<>]*)(/<.*>)*/?"
    rv = []
    for route in app.url_map.iter_rules():
        rv.append(re.compile(base_url_regex).match(route.rule).group(1).rstrip("/"))
    return set(rv)
