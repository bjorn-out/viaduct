# coding=utf-8
from datetime import datetime

from dateutil.relativedelta import relativedelta
from flask_babel import lazy_gettext as _  # noqa
from flask_wtf import FlaskForm
from flask_wtf.recaptcha import Recaptcha, RecaptchaField
from wtforms import (
    BooleanField,
    DateField,
    Field,
    FileField,
    PasswordField,
    SelectField,
    StringField,
)
from wtforms.ext.sqlalchemy.fields import QuerySelectMultipleField
from wtforms.validators import EqualTo, InputRequired, Length, Optional, ValidationError

from app import constants, get_locale
from app.forms.fields import EmailField
from app.forms.util import FieldVerticalSplit
from app.models.education import Education
from app.service import education_service


class ContextMacroCall(Field):
    def __init__(self, macro_name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.macro_name = macro_name


class ResetPasswordForm(FlaskForm):
    password = PasswordField(
        _("New password"),
        validators=[
            InputRequired(),
            Length(
                message=(
                    _(
                        "Minimal password length: %(length)d",
                        length=constants.MIN_PASSWORD_LENGTH,
                    )
                ),
                min=constants.MIN_PASSWORD_LENGTH,
            ),
        ],
    )
    password_repeat = PasswordField(
        _("Repeat new password"),
        validators=[
            InputRequired(),
            EqualTo("password", message=_("Passwords do not match")),
        ],
    )


def get_education_name(education: Education) -> str:
    return education_service.get_education_name(education, get_locale())


class StudentInformation:
    student_info = ContextMacroCall("render_student_info")


class UnconfirmedStudentIDForm(FlaskForm):
    student_id = StringField(_("Student ID"))
    educations = QuerySelectMultipleField(
        _("Study programmes"),
        validators=[InputRequired()],
        query_factory=education_service.get_all_educations,
        get_label=get_education_name,
        get_pk=lambda e: e.id,
    )


class BaseUserForm(FlaskForm):
    email = EmailField(
        _("E-mail adress"), validators=[InputRequired(), Length(max=200)]
    )
    first_name = StringField(
        _("First name"), validators=[InputRequired(), Length(max=256)]
    )
    last_name = StringField(
        _("Last name"), validators=[InputRequired(), Length(max=256)]
    )

    address = StringField(_("Address"), validators=[InputRequired(), Length(max=256)])
    zip = StringField(_("Zip code"), validators=[InputRequired(), Length(max=8)])
    city = StringField(_("City"), validators=[InputRequired(), Length(max=256)])
    country = StringField(
        _("Country"), default="Nederland", validators=[InputRequired(), Length(max=256)]
    )

    # Dates
    birth_date = DateField(_("Birthdate"), validators=[InputRequired()])
    study_start = DateField(_("Starting year study"), validators=[InputRequired()])

    # Optional fields
    locale = SelectField(
        _("Language"),
        validators=[Optional()],
        choices=list(constants.LANGUAGES.items()),
        default="",
    )
    phone_nr = StringField(_("Phone"), validators=[Optional(), Length(max=16)])
    avatar = FileField("Avatar", validators=[Optional()])


class SAMLSignUpForm(BaseUserForm):
    birth_date = DateField(_("Birthdate"), validators=[InputRequired()])
    study_start = DateField(_("Starting year study"), validators=[InputRequired()])
    recaptcha = RecaptchaField(validators=[Recaptcha(message="Check Recaptcha")])
    agree_with_privacy_policy = BooleanField(
        validators=[InputRequired(_("Please agree with our Privacy Policy."))]
    )

    register_split = FieldVerticalSplit(
        [
            ["first_name", "last_name", "birth_date", "email", "study_start"],
            ["address", "zip", "city", "country"],
        ],
        large_spacing=True,
    )

    _RenderIgnoreFields = [
        "locale",
        "phone_nr",
        "avatar",
        "agree_with_privacy_policy",
        "recaptcha",
    ]

    def validate_birth_date(self, field):  # pylint: disable=no-self-use
        sixteen_years_ago = datetime.now().date() - relativedelta(years=16)

        if field.data is None:
            return

        if field.data > sixteen_years_ago:
            raise ValidationError(_("You need to be at least 16 years old."))


class ManualSignUpForm(SAMLSignUpForm, UnconfirmedStudentIDForm, ResetPasswordForm):
    register_split = FieldVerticalSplit(
        [
            [
                "first_name",
                "last_name",
                "birth_date",
                "address",
                "zip",
                "city",
                "country",
            ],
            [
                "email",
                "password",
                "password_repeat",
                "student_id",
                "educations",
                "study_start",
            ],
        ],
        large_spacing=True,
    )


class EditUserForm(BaseUserForm, StudentInformation):
    """Edit a user as administrator."""

    register_split = FieldVerticalSplit(
        [
            ["first_name", "last_name", "address", "zip", "city", "country"],
            ["email", "birth_date", "study_start", "student_info"],
        ],
        large_spacing=True,
    )

    optional_split = FieldVerticalSplit(
        [["phone_nr", "locale"], ["avatar"]], large_spacing=True
    )

    alumnus = BooleanField(_("Alumnus"))


class SignInForm(FlaskForm):
    email = EmailField(_("E-mail address"), validators=[InputRequired()])
    password = PasswordField(_("Password"), validators=[InputRequired()])


class SignInOtpForm(FlaskForm):
    otp = StringField(
        _("Authentication code"),
        validators=[InputRequired()],
        render_kw={
            "pattern": "[0-9]*",
            "autocomplete": "one-time-code",
            "inputmode": "numeric",
        },
    )


class RequestPassword(FlaskForm):
    email = EmailField(_("E-mail address"), validators=[InputRequired()])
    recaptcha = RecaptchaField(validators=[Recaptcha(message="Check Recaptcha")])


class ChangePasswordForm(ResetPasswordForm):
    current_password = PasswordField(
        _("Current Password"),
        validators=[
            InputRequired(),
        ],
    )


class EditUvALinkingForm(FlaskForm):
    student_id = StringField(_("Student ID"), validators=[Optional()])
    student_id_confirmed = BooleanField(
        _("Link this account to the corresponding UvA account")
    )

    def validate_student_id(self, field):
        if self.student_id_confirmed.data and not field.data:
            raise ValidationError(
                _(
                    "A student ID is required when this account"
                    " is linked to a UvA account."
                )
            )
