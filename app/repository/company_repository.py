from datetime import date, datetime
from typing import List, Optional

from flask_sqlalchemy import Pagination
from sqlalchemy import and_

from app import db
from app.api.schema import PageJobSearchParameters
from app.models.company import Company
from app.models.company_banner import CompanyBanner
from app.models.company_job import CompanyJob
from app.models.company_profile import CompanyProfile
from app.repository.utils.pagination import search_columns


def find_company_by_id(company_id: int) -> Optional[Company]:
    return db.session.query(Company).filter(Company.id == company_id).one_or_none()


def find_company_by_slug(company_slug: str) -> Optional[Company]:
    return db.session.query(Company).filter(Company.slug == company_slug).one_or_none()


def find_job_by_id(job_id: int) -> Optional[CompanyJob]:
    return db.session.query(CompanyJob).filter(CompanyJob.id == job_id).one_or_none()


def find_all_companies(fitler_inactive: bool) -> List[Company]:
    q = db.session.query(Company)
    if fitler_inactive:
        q = q.filter(
            and_(
                Company.contract_start_date < date.today(),
                Company.contract_end_date > date.today(),
            )
        )
    return q.all()


def find_banner_by_company_id(company_id: int) -> Optional[CompanyBanner]:
    return (
        db.session.query(CompanyBanner)
        .filter(CompanyBanner.company_id == company_id)
        .one_or_none()
    )


def find_profile_by_company_id(company_id: int) -> Optional[CompanyProfile]:
    return (
        db.session.query(CompanyProfile)
        .filter(CompanyProfile.company_id == company_id)
        .one_or_none()
    )


def edit_company(
    company_id: int,
    name: str,
    slug: str,
    website: str,
    contract_start_date: date,
    contract_end_date: date,
):
    query = (
        db.update(Company)
        .where(Company.id == company_id)
        .values(
            name=name,
            slug=slug,
            website=website,
            contract_start_date=contract_start_date,
            contract_end_date=contract_end_date,
        )
    )
    db.session.execute(query)
    db.session.commit()


def edit_company_job(
    job_id: int,
    enabled: bool,
    start_date: date,
    end_date: date,
    title_nl: str,
    title_en: str,
    description_nl: str,
    description_en: str,
    contact_name: str,
    contact_email: str,
    contact_address: str,
    contact_city: str,
    website: str,
    phone: str,
    contract_of_service: str,
):
    query = (
        db.update(CompanyJob)
        .where(CompanyJob.id == job_id)
        .values(
            enabled=enabled,
            start_date=start_date,
            end_date=end_date,
            title_nl=title_nl,
            title_en=title_en,
            description_nl=description_nl,
            description_en=description_en,
            contact_name=contact_name,
            contact_email=contact_email,
            contact_address=contact_address,
            contact_city=contact_city,
            website=website,
            phone=phone,
            contract_of_service=contract_of_service,
        )
    )

    db.session.execute(query)
    db.session.commit()


def edit_company_banner(
    company_id: int, start_date: date, end_date: date, enabled: bool, website: str
):
    query = (
        db.update(CompanyBanner)
        .where(CompanyBanner.company_id == company_id)
        .values(
            start_date=start_date, end_date=end_date, website=website, enabled=enabled
        )
    )
    db.session.execute(query)
    db.session.commit()


def edit_company_profile(
    company_id: int,
    start_date: date,
    end_date: date,
    enabled: bool,
    description_nl: str,
    description_en: str,
):
    query = (
        db.update(CompanyProfile)
        .where(CompanyProfile.company_id == company_id)
        .values(
            start_date=start_date,
            end_date=end_date,
            enabled=enabled,
            description_nl=description_nl,
            description_en=description_en,
        )
    )
    db.session.execute(query)
    db.session.commit()


def set_logo_file_id(company_id: int, file_id: int):
    query = (
        db.update(Company).where(Company.id == company_id).values(logo_file_id=file_id)
    )
    db.session.execute(query)
    db.session.commit()


def create_company(
    name: str,
    slug: str,
    website: str,
    contract_start_date: date,
    contract_end_date: date,
):
    company = Company(
        name=name,
        slug=slug,
        website=website,
        contract_start_date=contract_start_date,
        contract_end_date=contract_end_date,
    )
    db.session.add(company)
    db.session.commit()
    return company


def create_job(
    company: Company,
    enabled: bool,
    start_date: date,
    end_date: date,
    title_nl: str,
    title_en: str,
    description_nl: str,
    description_en: str,
    contact_name: str,
    contact_email: str,
    contact_address: str,
    contact_city: str,
    website: str,
    phone: str,
    contract_of_service: str,
) -> CompanyJob:
    job = CompanyJob(
        company=company,
        enabled=enabled,
        start_date=start_date,
        end_date=end_date,
        title_nl=title_nl,
        title_en=title_en,
        description_nl=description_nl,
        description_en=description_en,
        contact_name=contact_name,
        contact_email=contact_email,
        contact_address=contact_address,
        contact_city=contact_city,
        website=website,
        phone=phone,
        contract_of_service=contract_of_service,
    )
    db.session.add(job)
    db.session.commit()
    return job


def create_company_banner(
    company_id: int,
    start_date: date,
    end_date: date,
    enabled: bool,
    website: Optional[str],
) -> CompanyBanner:
    banner = CompanyBanner(
        company_id=company_id,
        enabled=enabled,
        start_date=start_date,
        end_date=end_date,
        website=website,
    )
    db.session.add(banner)
    db.session.commit()
    return banner


def create_company_profile(
    company_id: int,
    start_date: date,
    end_date: date,
    enabled: bool,
    description_nl: str,
    description_en: str,
) -> CompanyProfile:
    profile = CompanyProfile(
        company_id=company_id,
        start_date=start_date,
        end_date=end_date,
        enabled=enabled,
        description_nl=description_nl,
        description_en=description_en,
    )
    db.session.add(profile)
    db.session.commit()
    return profile


def company_name_exists(name: str, current_company: Company = None) -> bool:
    q = db.session.query(Company).filter(Company.name == name)
    if current_company is not None:
        q = q.filter(Company.id != current_company.id)

    return q.count() > 0


def company_slug_exists(slug: str, current_company: Company = None) -> bool:
    q = db.session.query(Company).filter(Company.slug == slug)
    if current_company is not None:
        q = q.filter(Company.id != current_company.id)

    return q.count() > 0


def _company_contract_active_filter():
    return (
        Company.contract_start_date <= datetime.today(),
        Company.contract_end_date >= datetime.today(),
    )


def _company_module_active_filter(cls):
    return (
        cls.start_date <= datetime.today(),
        cls.end_date >= datetime.today(),
        cls.enabled.is_(True),
    )


def find_all_jobs(filter_inactive: bool) -> List[CompanyJob]:
    q = db.session.query(CompanyJob)

    if filter_inactive:
        q = q.join(Company)
        q = q.filter(
            *_company_contract_active_filter(),
            *_company_module_active_filter(CompanyJob)
        )
    return q.all()


def paginated_search_all_jobs(
    pagination: PageJobSearchParameters,
    filter_inactive: bool,
) -> Pagination:
    q = db.session.query(CompanyJob).filter(
        CompanyJob.contract_of_service.in_(pagination.contract_of_service)
    )

    if filter_inactive:
        q = q.join(Company)
        q = q.filter(
            *_company_contract_active_filter(),
            *_company_module_active_filter(CompanyJob)
        )

    q = search_columns(
        q,
        pagination.search,
        CompanyJob.title_en,
        CompanyJob.title_nl,
        CompanyJob.description_en,
        CompanyJob.description_nl,
        CompanyJob.contact_address,
        CompanyJob.contact_city,
        Company.name,
    )

    q = q.order_by(CompanyJob.ordering)

    return q.paginate(pagination.page, pagination.limit, False)


def find_all_contract_of_services(filter_inactive: bool) -> List[str]:
    q = db.session.query(CompanyJob.contract_of_service)

    if filter_inactive:
        q = q.filter(*_company_contract_active_filter(), CompanyJob.enabled.is_(True))

    return q.distinct().all()


def find_all_banners(filter_inactive: bool) -> List[CompanyBanner]:
    q = db.session.query(CompanyBanner)

    if filter_inactive:
        q = q.join(Company)
        q = q.filter(
            *_company_contract_active_filter(),
            *_company_module_active_filter(CompanyBanner)
        )
    return q.all()


def find_all_profiles(filter_inactive: bool) -> List[CompanyProfile]:
    q = db.session.query(CompanyProfile)

    if filter_inactive:
        q = q.join(Company)
        q = q.filter(
            *_company_contract_active_filter(),
            *_company_module_active_filter(CompanyProfile)
        )
    return q.all()


def find_jobs_by_company_id(company_id: int, filter_inactive: bool) -> List[CompanyJob]:
    q = db.session.query(CompanyJob).filter(CompanyJob.company_id == company_id)

    if filter_inactive:
        q = q.filter(
            CompanyJob.start_date <= datetime.today(),
            CompanyJob.end_date >= datetime.today(),
            CompanyJob.enabled.is_(True),
        )
    return q.all()
