import datetime
from typing import List, Optional, Tuple

from sqlalchemy import and_

from app import db
from app.models.challenge import Challenge, Submission


def find_with_approved_submission(
    closed, user_id
) -> List[Tuple[Challenge, Optional[Submission]]]:
    q = db.session.query(Challenge, Submission).outerjoin(
        Submission,
        and_(
            Challenge.id == Submission.challenge_id,
            Submission.user_id == user_id,
            Submission.approved.is_(True),
        ),
    )
    if not closed:
        q = q.filter(
            Challenge.start_date <= datetime.date.today(),
            Challenge.end_date >= datetime.date.today(),
        )

    return q.all()
