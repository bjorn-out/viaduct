from typing import List, Optional

from flask_sqlalchemy import Pagination
from sqlalchemy import select

from app import db
from app.api.schema import PageSearchParameters
from app.models.course import EducationCourse
from app.models.education import Education
from app.models.user import UserEducation
from app.repository.utils.pagination import search_columns


def get_education_by_id(education_id: int) -> Optional[Education]:
    return db.session.query(Education).filter_by(id=education_id).one_or_none()


def get_education_by_en_name(name: str) -> Optional[Education]:
    return db.session.query(Education).filter_by(en_name=name).one_or_none()


def get_education_by_nl_name(name: str) -> Optional[Education]:
    return db.session.query(Education).filter_by(nl_name=name).one_or_none()


def get_educations_by_user_id(user_id: int) -> List[Education]:
    stmt = (
        select(Education).join(UserEducation).filter(UserEducation.user_id == user_id)
    )
    educations = db.session.execute(stmt)
    return educations.scalars().all()


def get_education_duration(education_id: int) -> int:
    """ Return the year of the latest bounded course, or 0. """
    course_durations = (
        db.session.query(EducationCourse.year)
        .filter(EducationCourse.education_id == education_id)
        .all()
    )

    course_durations = [cd[0] for cd in course_durations if cd[0] is not None]

    if course_durations:
        return max(course_durations)
    else:
        return 0


def find_by_datanose_code(datanose_code: str) -> Optional[Education]:
    return (
        db.session.query(Education).filter_by(datanose_code=datanose_code).one_or_none()
    )


def get_all_educations() -> List[Education]:
    return db.session.query(Education).order_by(Education.en_name).all()


def get_all_via_educations() -> List[Education]:
    return (
        db.session.query(Education)
        .filter_by(is_via_programme=True)
        .order_by(Education.en_name)
        .all()
    )


def paginated_search_all_educations(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(Education).order_by(Education.en_name.asc())
    q = search_columns(q, pagination.search, Education.nl_name, Education.en_name)

    return q.paginate(pagination.page, pagination.limit, error_out=False)


def create_education() -> Education:
    return Education()


def save_education(education: Education):
    db.session.add(education)
    db.session.commit()


def delete_education(education_id: int):
    education = db.session.query(Education).filter_by(id=education_id).first()
    db.session.delete(education)
    db.session.commit()


def count_courses_by_education_id(education_id: int) -> int:
    course_ids = (
        db.session.query(EducationCourse.course_id)
        .filter(EducationCourse.education_id == education_id)
        .all()
    )
    return len(course_ids)
