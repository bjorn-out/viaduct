from typing import Optional

from app import db
from app.models.setting_model import Setting


def create_setting() -> Setting:
    return Setting()


def save(setting: Setting) -> Setting:
    db.session.add(setting)
    db.session.commit()
    return setting


def find_by_key(key) -> Optional[Setting]:
    return db.session.query(Setting).filter_by(key=key).first()


def delete_setting(setting: Setting):
    db.session.delete(setting)
    db.session.commit()
