from typing import List, Optional

from werkzeug.exceptions import Gone

from app.api.schema import PageSearchParameters
from app.exceptions.base import (
    DuplicateResourceException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.page import Page, PageRevision
from app.models.user import User
from app.repository import page_repository
from app.roles import Roles
from app.service import navigation_service, role_service
from app.utils.pagination import Pagination
from app.service.markdown_service import render_markdown_safely


def can_user_read_page(page, user):
    """Test whether a use can read the page."""
    if role_service.user_has_role(user, Roles.PAGE_WRITE):
        return True

    # If the page needs membership, the user has_paid has to be true.
    return not page.needs_paid or user.has_paid


def get_page_by_id(page_id, incl_deleted=False):
    page = page_repository.find_page_by_id(page_id)
    if page is None:
        raise ResourceNotFoundException("page", page_id)
    if page.deleted and not incl_deleted:
        raise Gone()
    return page


def get_page_by_path(path, incl_deleted=False) -> Page:
    page = page_repository.find_page_by_path(path)
    if page is None:
        raise ResourceNotFoundException("page", path)
    if page.deleted and not incl_deleted:
        raise Gone()
    return page


def delete_page_by_path(path):
    page = page_repository.find_page_by_path(path)
    if not page or page.deleted:
        return False
    else:
        page_repository.delete_page(page)
    return True


def paginated_search_all_pages(pagination: PageSearchParameters) -> Pagination:
    """Get all pages with search and pagination."""
    return page_repository.paginated_search_all_pages(pagination)


def get_page_text_for_path(page_path):
    try:
        return preview_latest_revision(get_page_by_path(page_path))
    except (ResourceNotFoundException, Gone):
        return "<i>Please edit the page {} to set a text here.</i>".format(page_path)


def preview_content(content: str):
    return render_markdown_safely(content)


def preview_latest_revision(page: Page) -> str:
    latest_rev = get_latest_revision(page)
    if not latest_rev:
        raise ResourceNotFoundException("page", page.id)

    return preview_content(latest_rev.content)


def create_page(page_type: str, path: str, require_membership_to_view: bool) -> Page:
    if page_repository.find_page_by_path(path) is not None:
        raise DuplicateResourceException("page", path)
    if (
        page_type == "page"
        and is_committee_path(path)
        or page_type == "committee"
        and not is_committee_path(path)
    ):
        raise ValidationException(
            f"Cannot create page type '{page_type}' " f"with path '{path}'"
        )

    return page_repository.create_page(page_type, path, require_membership_to_view)


def edit_page(page: Page, path: str, require_membership_to_view: bool) -> Page:
    if page.path != path:
        if page_repository.find_page_by_path(path) is not None:
            raise DuplicateResourceException("page", path)

        if (
            page.type == "page"
            and is_committee_path(path)
            or page.type == "committee"
            and not is_committee_path(path)
        ):
            raise ValidationException(
                f"Cannot change page type '{page.type}' " f"to path '{path}'"
            )
    return page_repository.update_page(page, path, require_membership_to_view)


def create_page_revision(
    page: Page,
    title,
    revision_comment: str,
    content,
    author: User,
) -> PageRevision:
    return page_repository.create_page_revision(
        page=page,
        author=author,
        title=title,
        content=content,
        revision_comment=revision_comment,
    )


def get_all_revisions(page: Page) -> List[PageRevision]:
    return page_repository.get_all_revisions(page)


def get_latest_revision(page: Page) -> Optional[PageRevision]:
    rev = page_repository.get_latest_revision(page)
    if rev is None:
        raise ResourceNotFoundException("page_revision", page.id)
    return rev


def get_latest_revisions(pages: List[Page]) -> List[PageRevision]:
    return page_repository.get_latest_revisions(pages)


def delete_page(page):
    navigation_entry = navigation_service.find_entry_by_path(page.path)
    if navigation_entry:
        navigation_service.delete_navigation_entry(navigation_entry, remove_page=False)
    page_repository.delete_page(page)


def get_revision_by_id(page: Page, revision_id: int) -> Optional[PageRevision]:
    return page_repository.get_revision_by_id(page, revision_id)


def is_committee_path(path: str):
    if path[0] == "/":
        path = path[1:]
    return path.startswith("commissie/")


def find_all_committee_pages() -> List[Page]:
    return page_repository.find_all_committees()


def find_all_active_pages() -> List[Page]:
    return page_repository.find_all_active_pages()


PageRevisions = List[PageRevision]


def find_latest_revisions_containing(
    search: str,
) -> PageRevisions:
    return page_repository.find_latest_page_revisions_containing(search)
