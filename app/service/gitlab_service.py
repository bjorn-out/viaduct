import enum
from typing import Literal
from unittest.mock import Mock

import requests
from flask import url_for

from app import app
from app.exceptions.base import ServiceUnavailableException
from app.models.user import User
from app.repository import gitlab_repository


class GitlabProjectEnum(enum.IntEnum):
    viaduct = 4110282
    pos = 6644893
    pretix = 14120498


def create_gitlab_session(token):
    session = requests.Session()
    session.headers.update({"PRIVATE-TOKEN": token})
    return session


def create_gitlab_issue(
    project_name: Literal["viaduct", "pretix", "pos"],
    summary: str,
    user: User,
    description: str,
) -> str:
    project = GitlabProjectEnum[project_name]
    # With UserSelfConverter the flask.url_for(user=current_user) uses "self" in urls.
    # We use a mock user to ensure the identity check fails and the id is used instead.
    mock_user = Mock(spec_set=("id",))
    mock_user.id = user.id
    description = (
        f"Bug report by viaduct ID: [{user.id}]"
        f"({url_for('user.view_single_user', user=mock_user, _external=True)})"
        f"\n\n{description}"
    )
    data = {"title": summary, "description": description, "labels": "viaduct"}
    with create_gitlab_session(app.config["GITLAB_TOKEN"]) as s:
        try:
            resp = gitlab_repository.create_gitlab_issue(s, project.value, data)
            resp.raise_for_status()
            return resp.json()
        except requests.RequestException as e:
            raise ServiceUnavailableException("Gitlab unavailable") from e
