from typing import List

import requests

DEPARTURES_URL = "https://api.9292.nl/0.1/locations/{}" "/departure-times?lang=nl-NL"


class Departure:
    def __init__(self, data):
        self.cancelled = data["realtimeState"] == "cancelled"
        self.time = data["time"]
        self.realtime_text = data["realtimeText"]
        if self.realtime_text:
            self.realtime_text = self.realtime_text.replace("min.", "")
        self.platform = data["platform"]
        self.service = data["service"]
        self.destination_name = data["destinationName"]
        self.remark = ""

        if self.cancelled:
            self._add_remark("Geannuleerd")
        if data["viaNames"]:
            self._add_remark(data["vianames"])
        if data["remark"]:
            self._add_remark(data["remark"])

    def _add_remark(self, remark):
        if self.remark:
            self.remark += ", "
        self.remark += remark


class Departures:
    def __init__(self, dep_type: str, name: str, departures: List):
        self.dep_type = dep_type
        self.name = name
        self.departures = [Departure(i) for i in departures]


def fetch_departures(dep_type: str, location: str):
    req = requests.get(DEPARTURES_URL.format(location))
    req.raise_for_status()
    data = req.json()
    name = data["location"]["name"]
    deps = data["tabs"][0]["departures"]
    if not name or not deps:
        return None
    return Departures(dep_type, name, deps)
