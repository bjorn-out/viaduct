import json
import logging
import os
from collections import namedtuple
from datetime import datetime, timedelta
from functools import wraps
from typing import List, NamedTuple, Optional
from urllib.parse import urlparse

from flask import request, session
from flask_login import current_user
from onelogin.saml2.auth import OneLogin_Saml2_Auth
from onelogin.saml2.constants import OneLogin_Saml2_Constants
from onelogin.saml2.idp_metadata_parser import OneLogin_Saml2_IdPMetadataParser
from onelogin.saml2.settings import OneLogin_Saml2_Settings
from onelogin.saml2.utils import OneLogin_Saml2_Utils

from app import app
from app.exceptions.base import (
    AuthorizationException,
    BusinessRuleException,
    ServiceUnavailableException,
    ValidationException,
)
from app.models.education import Education
from app.service import datanose_service, education_service, user_service

_logger = logging.getLogger(__name__)
SAMLInfo = namedtuple("SAMLInfo", ["auth", "req"])

# Session keys
SESSION_SAML_DATA = "saml_data"
SESSION_SAML_SIGN_UP_SESSION = "saml_sign_up_session"

# SAML data dict keys
SAML_DATA_IS_AUTHENTICATED = "is_authenticated"
SAML_DATA_ATTRIBUTES = "attributes"
SAML_DATA_NAMEID = "nameid"
SAML_DATA_LINKING_USER_ID = "linking_user_id"


# SAML sign up session dict keys and timeout
class SAMLSignUpSession(NamedTuple):
    time_touched: datetime
    given_name: str
    surname: str
    student_id: str
    email: str
    locale: str
    study_programme_codes: List[str]


SIGN_UP_SESSION_TIMEOUT = timedelta(minutes=5)

# SAML attribute URNs.
# See this wiki page for a more detailed description of the attributes:
# https://wiki.surfnet.nl/display/surfconextdev/Attributes

# Examples: '12345678' (in case of a student), 'jdoe1' (in case of an employee)
ATTR_URN_UID = "urn:mace:dir:attribute-def:uid"

# Example: ['student', 'member', 'affiliate']
ATTR_URN_PERSON_AFFILIATION = "urn:mace:dir:attribute-def:eduPersonAffiliation"

# Examples: 'John', 'John Davison'
ATTR_URN_GIVEN_NAME = "urn:mace:dir:attribute-def:givenName"

# Example: 'Doe', 'van der Sanden'
ATTR_URN_SURNAME = "urn:mace:dir:attribute-def:sn"

# Examples: 'john.doe@student.uva.nl', 'j.d.doe@uva.nl'
ATTR_URN_MAIL = "urn:mace:dir:attribute-def:mail"

# Examples: 'nl', 'en'
ATTR_URN_PREFERRED_LANGUAGE = "urn:mace:dir:attribute-def:preferredLanguage"


def _requires_saml_data(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if SESSION_SAML_DATA not in session:
            session[SESSION_SAML_DATA] = {}

        try:
            return f(*args, **kwargs)
        finally:
            # Explicitly tell the session that it has been modified,
            # since just adding/removing an item in the session['saml_data']
            # dict is not picked up as no items in the session dict itself
            # are changed. See:
            # http://flask.pocoo.org/docs/1.0/api/?highlight=session#flask.session
            session.modified = True

    return wrapper


def _requires_active_sign_up_session(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not get_active_sign_up_session():
            raise ValidationException("No SAML sign up session active.")

        try:
            return f(*args, **kwargs)
        finally:
            session.modified = True

    return wrapper


def ensure_data_cleared(f):
    """
    Decorator that requires that clear_saml_data is called after the
    view method returns, which clears the saml signup data from the
    session.
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        finally:
            clear_saml_data()

    return wrapper


def build_saml_info():
    url_data = urlparse(request.url)

    saml_req = {
        "http_host": request.host,
        "server_port": url_data.port,
        "script_name": request.path,
        "get_data": request.args.copy(),
        "post_data": request.form.copy(),
        "https": "on",
    }

    # This is the base path for all SAML config files and certificates
    # It is equal to "saml/{develop,master}" and is structured as follows:
    # saml/{develop,master}/
    #   settings.json:              General settings
    #   advanced_settings.json:     Some extra settings
    #   certs/sp.{key,crt}          Certificate and private key

    saml_path = app.config["SAML_PATH"]

    # Unfortunately, we have to load these on our own...
    base_settings = {}
    with open(os.path.join(saml_path, "settings.json")) as f_settings:
        base_settings.update(json.load(f_settings))
    with open(os.path.join(saml_path, "advanced_settings.json")) as f_settings:
        base_settings.update(json.load(f_settings))

    # Get the URL of the IdP metadata. Note that this is a custom
    # settings key which is just ignored by the OneLogin SAML library.
    idp_metadata_url = base_settings["idp"]["metadataUrl"]

    # Get IdP settings from metadata
    idp_settings = OneLogin_Saml2_IdPMetadataParser.parse_remote(
        idp_metadata_url,
        required_sso_binding=OneLogin_Saml2_Constants.BINDING_HTTP_REDIRECT,
    )

    # The IdP metadata parser extracts the first available NameIDFormat
    # from the IdP metadata. Since we specify
    # urn:oasis:names:tc:SAML:2.0:nameid-format:persistent in settings.json,
    # we remove this to avoid it being overwritten in the merge.
    del idp_settings["sp"]["NameIDFormat"]

    # Merge IdP and base settings
    settings_dict = OneLogin_Saml2_IdPMetadataParser.merge_settings(
        base_settings, idp_settings
    )

    # Create settings object which also loads our certificates
    settings = OneLogin_Saml2_Settings(settings_dict, custom_base_path=saml_path)

    # Finally, construct the Auth object
    saml_auth = OneLogin_Saml2_Auth(saml_req, settings)
    return SAMLInfo(saml_auth, saml_req)


@_requires_saml_data
def process_response(saml_info):
    saml_auth = saml_info.auth

    saml_auth.process_response()
    errors = saml_auth.get_errors()

    if errors:
        msg = (
            "One or more errors occurred during SAML response processing: "
            + saml_auth.get_last_error_reason()
        )

        _logger.error(msg)
        raise ValidationException(msg)

    if saml_auth.is_authenticated():
        session[SESSION_SAML_DATA][SAML_DATA_IS_AUTHENTICATED] = True
        session[SESSION_SAML_DATA][SAML_DATA_ATTRIBUTES] = saml_auth.get_attributes()
        session[SESSION_SAML_DATA][SAML_DATA_NAMEID] = saml_auth.get_nameid()
    else:
        session[SESSION_SAML_DATA][SAML_DATA_IS_AUTHENTICATED] = False


def get_relaystate_redirect_url(saml_info, fallback_url):
    saml_auth = saml_info.auth

    if (
        "RelayState" in request.form
        and OneLogin_Saml2_Utils.get_self_url(saml_info.req)
        != request.form["RelayState"]
    ):
        return saml_auth.redirect_to(request.form["RelayState"])

    return fallback_url


def initiate_login(saml_info, return_to, force_authn=False):
    return saml_info.auth.login(return_to=return_to, force_authn=force_authn)


def build_metadata(saml_info):
    settings = saml_info.auth.get_settings()
    metadata = settings.get_sp_metadata()
    errors: list = settings.validate_metadata(metadata)

    if not errors:
        return metadata
    else:
        raise ValidationException(
            "Errors occurred when building SAML metadata: {}".format(", ".join(errors))
        )


@_requires_saml_data
def set_linking_user(user):
    session[SESSION_SAML_DATA][SAML_DATA_LINKING_USER_ID] = user.id


@_requires_saml_data
def is_linking_user_current_user():
    user_id = session[SESSION_SAML_DATA].get(SAML_DATA_LINKING_USER_ID)
    return user_id is None or user_id == current_user.id


@_requires_saml_data
def get_linking_user():
    user_id = session[SESSION_SAML_DATA].get(SAML_DATA_LINKING_USER_ID)
    if not user_id:
        return current_user

    return user_service.get_user_by_id(user_id)


@_requires_saml_data
def user_is_authenticated():
    return session[SESSION_SAML_DATA].get(SAML_DATA_IS_AUTHENTICATED, False)


@_requires_saml_data
def get_attributes():
    return session[SESSION_SAML_DATA].get(SAML_DATA_ATTRIBUTES, {})


def get_uid_from_attributes():
    attributes = get_attributes()

    value = attributes.get(ATTR_URN_UID)
    if not value:
        return None

    # Since attributes are always lists, we use [0] to
    # pick the first and only value.
    return value[0]


def get_user_by_uid(needs_confirmed=True):
    uid = get_uid_from_attributes()

    if not uid:
        raise ValidationException("uid not found in SAML attributes")

    user = user_service.get_user_by_student_id(uid, needs_confirmed)

    if user.disabled:
        raise AuthorizationException("User is disabled.")

    return user


def uid_is_linked_to_other_user():
    uid = get_uid_from_attributes()
    if not uid:
        raise ValidationException("uid not found in SAML attributes")

    other_user = user_service.find_user_by_student_id(uid)
    return other_user is not None


def user_is_student():
    attributes = get_attributes()
    affiliation = attributes.get(ATTR_URN_PERSON_AFFILIATION)

    if not affiliation:
        return False

    return "student" in affiliation


def link_uid_to_user(user):
    uid = get_uid_from_attributes()
    if not uid:
        raise ValidationException("uid not found in SAML attributes")

    if not user_is_student():
        raise BusinessRuleException("Authenticated user is not a student.")

    user_service.set_confirmed_student_id(user, uid)


@_requires_saml_data
def get_nameid():
    return session[SESSION_SAML_DATA].get(SAML_DATA_NAMEID)


@_requires_saml_data
def clear_saml_data():
    del session[SESSION_SAML_DATA]


def start_sign_up_session():
    student_id = get_uid_from_attributes()
    if not student_id:
        raise ValidationException("uid not found in SAML attributes")

    attributes = get_attributes()

    given_name = attributes.get(ATTR_URN_GIVEN_NAME)
    surname = attributes.get(ATTR_URN_SURNAME)
    email = attributes.get(ATTR_URN_MAIL)
    locale = attributes.get(ATTR_URN_PREFERRED_LANGUAGE)

    try:
        study_programme_codes = get_study_programmes_codes(student_id)
    except ServiceUnavailableException as e:
        raise BusinessRuleException("Cannot get study programmes") from e

    session[SESSION_SAML_SIGN_UP_SESSION] = SAMLSignUpSession(
        time_touched=datetime.now(),
        given_name=given_name[0] if given_name else "",
        surname=surname[0] if surname else "",
        student_id=student_id,
        email=email[0] if email else "",
        locale=locale[0] if locale else "en",
        study_programme_codes=study_programme_codes,
    )


def get_active_sign_up_session() -> Optional[SAMLSignUpSession]:
    tuple_saml_session = session.get(SESSION_SAML_SIGN_UP_SESSION)
    if not tuple_saml_session:
        return None

    if not isinstance(tuple_saml_session, SAMLSignUpSession):
        try:
            saml_session = SAMLSignUpSession(*tuple_saml_session)
        except TypeError:
            # Catch TypeError for old sign up sessions which
            # were created before without the SAMLSignUpSession class.
            # The call above will fail in that case, so we just return None.
            return None
    else:
        saml_session = tuple_saml_session

    if not saml_session:
        return None
    time_last_touched = datetime.now() - saml_session.time_touched
    if time_last_touched < SIGN_UP_SESSION_TIMEOUT:
        return saml_session
    return None


def get_study_programmes_codes(student_id) -> List[str]:
    dn_study_programmes = datanose_service.get_study_programmes(student_id)

    return [pr.code for pr in dn_study_programmes]


def get_study_programmes_by_codes(study_programme_codes: List[str]) -> List[Education]:
    return [
        pr
        for pr in (
            education_service.find_by_datanose_code(p) for p in study_programme_codes
        )
        if pr is not None
    ]


@_requires_active_sign_up_session
def update_sign_up_session_timestamp():
    saml_session: SAMLSignUpSession = get_active_sign_up_session()
    saml_session._replace(time_touched=datetime.now())


@_requires_active_sign_up_session
def end_sign_up_session():
    del session[SESSION_SAML_SIGN_UP_SESSION]
