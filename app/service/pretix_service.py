import enum
import json
import logging
from collections import defaultdict
from datetime import datetime
from decimal import Decimal
from typing import Dict, List, NamedTuple, Optional, Tuple, Any
from urllib.parse import urljoin

import requests
from cryptography import fernet
from requests.structures import CaseInsensitiveDict

from app import app
from app.networking import DefaultResponseAdapter
from app.models.user import User
from app.roles import Roles
from app.service import group_service, role_service, user_service
from config import Config

_logger = logging.getLogger(__name__)

PRETIX_ENABLED = True
PRETIX_SESSION = requests.Session()
PRETIX_BASE_URL = ""


class PretixEventRequest(NamedTuple):
    name: Dict[str, str]  # language -> event name
    date_from: datetime
    date_to: datetime
    quota_size: int
    group_id: int
    price: Decimal


@Config.post_load
def _pretix_config(config: Config):
    global PRETIX_BASE_URL, PRETIX_ENABLED
    PRETIX_ENABLED = config.PRETIX_ENABLED

    PRETIX_SESSION.mount(config.PRETIX_HOST, DefaultResponseAdapter("Pretix"))
    PRETIX_SESSION.headers = CaseInsensitiveDict(
        {
            "accept": "application/json",
            "content-Type": "application/json",
            "authorization": "Token " + config.PRETIX_TOKEN,
        }
    )
    PRETIX_BASE_URL = urljoin(
        config.PRETIX_HOST, "/api/v1/organizers/" + config.PRETIX_ORGANIZER + "/"
    )


def get_event_settings(slug) -> Dict[str, Any]:
    request_path = urljoin(PRETIX_BASE_URL, f"events/{slug}/settings/")

    resp = PRETIX_SESSION.get(request_path)
    return json.loads(resp.content)


def get_events_for_user(user: User, args: dict) -> bytes:
    request_path = urljoin(PRETIX_BASE_URL, "events/")
    pretix_args = args.copy()

    if not role_service.user_has_role(user, Roles.FORM_ADMIN):
        groups = group_service.get_groups_for_user(user)
        group_ids = {str(group.id) for group in groups}

        pretix_args["group_id"] = ",".join(group_ids)

    resp = PRETIX_SESSION.get(request_path, params=pretix_args)
    return json.loads(resp.content)["results"]


class PretixOrder(NamedTuple):
    code: str
    secret: str


class PretixPaymentStatus(enum.IntFlag):
    # Official pretix stati
    paid = 1
    pending = 2
    canceled = 4
    expired = 8

    # Often requested together.
    active = paid | pending

    def strings(self) -> Tuple[str, ...]:
        stati = {
            PretixPaymentStatus.paid: "p",
            PretixPaymentStatus.pending: "n",
            PretixPaymentStatus.canceled: "c",
            PretixPaymentStatus.expired: "e",
        }
        r = []
        for status in stati.keys():
            if status in self:
                r.append(stati[status])
        return tuple(r)


def get_user_order(
    event_slug, user, status: PretixPaymentStatus
) -> Optional[PretixOrder]:
    if not PRETIX_ENABLED:
        return None

    request_path = urljoin(
        PRETIX_BASE_URL, f"events/{event_slug}/via_users/{user.id}/orders/"
    )
    args = {"status__in": ",".join(status.strings())}
    resp = PRETIX_SESSION.get(request_path, params=args)
    if resp.status_code != 200:
        return None

    for order in resp.json():
        return PretixOrder(code=order["code"], secret=order["secret"])
    return None


def get_user_orders(user, status: PretixPaymentStatus) -> Dict[str, List[PretixOrder]]:
    if not PRETIX_ENABLED:
        return {}

    request_path = urljoin(PRETIX_BASE_URL, f"via_users/{user.id}/orders/")
    args = {"status__in": ",".join(status.strings())}
    try:
        resp = PRETIX_SESSION.get(request_path, params=args)
    except requests.exceptions.ConnectionError:
        _logger.debug("Could not request user pretix events", exc_info=True)
        return {}

    if resp.status_code != 200:
        return {}

    events: Dict[str, List[PretixOrder]] = defaultdict(list)
    for order in resp.json():
        events[order["event"]["slug"]].append(
            PretixOrder(code=order["code"], secret=order["secret"])
        )
    return events


def get_pretix_user_exchange_key():
    key = app.secret_key
    while len(key) < 44:
        key += key

    # Add "=" as base64 padding to make the base64 encoding 32 bytes.
    return key[:43] + "="


def get_fernet_token_from_user(user: User) -> str:
    key = get_pretix_user_exchange_key()
    return fernet.Fernet(key).encrypt(str(user.id).encode()).decode("utf-8")


def get_user_from_pretix_link(fernet_token: str) -> Optional[User]:
    key = get_pretix_user_exchange_key()
    try:
        user_id = int(
            fernet.Fernet(key).decrypt(fernet_token.encode(), ttl=1800).decode()
        )

        return user_service.find_by_id(user_id)
    except fernet.InvalidToken:
        return None
