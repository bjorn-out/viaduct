"""
What is pimpy?

Pimpy; Precise Inscriptions of Minutes with Python.
The minute system of the study association via.

Pimpy is used for handling the minutes and tasks of different groups.
By using different keywords in the minutes tasks are extracted and
automatically created.

The Minute(s) are the notes of a meeting where the written text is in.
With actions in these notes new Task(s) are created. These are bound to users,
and have a status.
"""
import logging
import re
from datetime import datetime, timedelta
from typing import List, Optional, Tuple

import baas32 as b32
import pytz
from fuzzywuzzy import fuzz

from app.enums import PimpyTaskStatus
from app.exceptions.base import (
    AuthorizationException,
    ResourceNotFoundException,
    ValidationException,
)
from app.exceptions.pimpy import InvalidMinuteException
from app.models.group import Group
from app.models.pimpy import Task, TaskUserRel
from app.models.user import User
from app.repository import pimpy_repository
from app.service import group_service, meeting_service

TASK_REGEX = re.compile(r"\s*(?:ACTIE|TASK)(?P<names> [^\n:]+)?:\s*(?P<task>.*)\s*$")
TASKS_REGEX = re.compile(r"\s*(?:ACTIES|TASKS)(?P<names> [^\n:]*)?:\s*(?P<task>.*)\s*$")
DONE_REGEX = re.compile(r"\s*(?:DONE) (?=.)([^\n\r]*)")
REMOVE_REGEX = re.compile(r"\s*(?:REMOVE) (?=.)([^\n\r]*)")
MISSING_COLON_REGEX = re.compile(r"\s*(?:ACTIE|TASK|ACTIES|TASKS)+[^:]*$")
MEETING_REGEX = re.compile(
    r"MEETING\s?(?P<date>([0-2]?\d|3["
    r"0-1])-(0?\d|1[0-2])-(\d{4}))?\s*"
    r"(?P<time>([0-1]?\d|2[0-3]):([0-5]\d))?"
    r"(\((?P<length>\d([.,]?\d)?)\))?\s*"
    r"(?P<title>.+)$"
)

_logger = logging.getLogger(__name__)


def get_all_minutes_for_user(user):
    return pimpy_repository.get_all_minutes_for_user(user)


def check_date_range(date_range):
    if date_range:
        if len(date_range) != 2:
            raise ValidationException("Date range should be of length 2")
        if not all(map(lambda x: isinstance(x, datetime), date_range)):
            raise ValidationException("Date range should consist of datetime")
        if date_range[0] > date_range[1]:
            raise ValidationException("First date should be smaller then second")


def get_task_by_b32_id(b32_task_id: str) -> Task:
    task = find_task_by_b32_id(b32_task_id)
    if not task:
        raise ResourceNotFoundException("task", b32_task_id)
    return task


def find_task_by_b32_id(b32_task_id: str) -> Optional[Task]:
    try:
        task_id = b32.decode(b32_task_id)
        return pimpy_repository.find_task_by_id(task_id)
    except ValueError:
        return None


def get_minutes_for_group(group: Group, date_range=None):
    check_date_range(date_range)

    return pimpy_repository.get_minutes_for_group(group, date_range)


def get_all_tasks_for_user(user, date_range=None):
    check_date_range(date_range)
    return pimpy_repository.get_all_tasks_for_user(user, date_range)


def get_all_tasks_for_group(
    group: Group, date_range=None, user=None
) -> List[TaskUserRel]:
    check_date_range(date_range)
    return pimpy_repository.get_all_tasks_for_group(group, date_range, user)


def get_all_tasks_for_users_in_groups_of_user(user, date_range=None):
    check_date_range(date_range)
    groups = group_service.get_groups_for_user(user)
    return pimpy_repository.get_all_tasks_for_users_in_groups(groups)


def check_user_can_access_task(user, task):
    if group_service.user_member_of_group(user, task.group_id):
        return
    if user in task.users:
        return

    raise AuthorizationException("User not member of group of task")


def check_user_can_access_minute(user, minute):
    if group_service.user_member_of_group(user, minute.group_id):
        return

    raise AuthorizationException("User not member of group of minute")


def set_task_status(task, status):
    valid = PimpyTaskStatus.NOT_STARTED.value <= status <= PimpyTaskStatus.MAX.value
    if not valid:
        raise ValidationException("Status not valid")

    return pimpy_repository.update_status(task, status)


def add_task_by_user_string(title, content, group_id, users_text, line, minute, status):
    group = group_service.get_by_id(group_id)
    user_list = get_list_of_users_from_string(group_id, users_text)

    return _add_task(
        title=title,
        content=content,
        group=group,
        user_list=user_list,
        minute=minute,
        line=line,
        status=status,
    )


def _add_task(title, content, group, user_list, line, minute, status):
    # Only check for tasks added not using minute.
    if not minute:
        task = pimpy_repository.find_task_by_name_content_group(title, content, group)

        if task:
            raise ValidationException("This task already exists")
    return pimpy_repository.add_task(
        title=title,
        content=content,
        group=group,
        users=user_list,
        minute=minute,
        line=line,
        status=status,
    )


def add_minute(content, date, group):
    # Parse the minute
    task_list, done_list, remove_list, meeting_list = _parse_minute(content, group)

    minute = pimpy_repository.add_minute(content=content, date=date, group=group)
    # Add new tasks
    for line, task, users in task_list:
        _add_task(task, "", group, users, line, minute, 0)

    # Mark existing tasks as done.
    for _, task in done_list:
        pimpy_repository.update_status(task, 4)

    # Mark existing tasks as removed.
    for _, task in remove_list:
        pimpy_repository.update_status(task, 5)

    for title, start, length in meeting_list:
        meeting_service.add(group.id, title, False, start, start + length)

    return minute


def edit_task_property(task, content=None, title=None, users_property=None):
    if content is not None:
        pimpy_repository.edit_task_content(task, content)

    if title is not None:
        pimpy_repository.edit_task_title(task, title)

    if users_property is not None:
        users = get_list_of_users_from_string(task.group_id, users_property)
        pimpy_repository.edit_task_users(task, users)


def get_list_of_users_from_string(group_id, comma_sep_users):
    """
    Get the list of users from a comma separated string of usernames.

    Parses a string which is a list of comma separated user names
    to a list of users, searching only within the group given

    :arg group_id is the group's id
    :arg comma_sep_users is a string with comma separated users.
    """
    group = group_service.get_by_id(group_id)

    if comma_sep_users is None:
        raise ValidationException("No comma separated list of users found.")

    comma_sep_users = comma_sep_users.strip()

    if not comma_sep_users:
        return group.users.all()

    comma_sep_users = filter(
        None, map(lambda x: x.lower().strip(), comma_sep_users.split(","))
    )

    users_found = []

    users = group.users.all()

    for comma_sep_user in comma_sep_users:
        maximum = 0
        match = None

        for user in users:
            rate_first = fuzz.ratio(user.first_name.lower(), comma_sep_user)
            rate_last = fuzz.ratio(user.last_name.lower(), comma_sep_user)
            rate_full = fuzz.ratio(user.name.lower(), comma_sep_user)

            new_max = max(rate_first, rate_last, rate_full)
            if new_max > maximum:
                maximum = new_max
                match = user

        if not match:
            raise ValidationException("Could not find a user for %s" % comma_sep_user)

        users_found.append(match)

    return users_found


NewTask = Tuple[int, str, List[User]]
RemoveTask = Tuple[int, Task]
DoneTask = Tuple[int, Task]
Meeting = Tuple[str, datetime, timedelta]
ParseResponse = Tuple[List[NewTask], List[DoneTask], List[RemoveTask], List[Meeting]]


def _parse_minute(content, group) -> ParseResponse:
    """
    Parse the specified minutes for tasks and return task, done and remove.

    Syntax within the content:
    ACTIE <name_1>, <name_2>, name_n>: <title of task>
    This creates a single task for one or multiple users

    ACTIES <name_1>, <name_2>, name_n>: <title of task>
    This creates one or multiple tasks for one or multiple users

    DONE <task1>, <task2, <taskn>
    This sets the given tasks on 'done'
    """
    missing_colon_lines = []
    unknown_task_ids = []
    unknown_user = []
    invalid_meetings = []

    task_list = []
    done_list = []
    remove_list = []
    meeting_list: List[Meeting] = []

    for i, line in enumerate(content.splitlines()):
        try:
            if MISSING_COLON_REGEX.search(line):
                missing_colon_lines.append((i, line))
                continue

            # Single task for multiple users.
            for names, task in TASK_REGEX.findall(line):
                users = get_list_of_users_from_string(group.id, names)
                task_list.append((i, task, users))

            # Single task for individual users.
            for names, task in TASKS_REGEX.findall(line):
                users = get_list_of_users_from_string(group.id, names)
                for user in users:
                    task_list.append((i, task, [user]))

            # Mark a comma separated list as done.
            for task_id_list in DONE_REGEX.findall(line):
                for b32_task_id in task_id_list.strip().split(","):
                    b32_task_id = b32_task_id.strip()
                    try:
                        task_id = b32.decode(b32_task_id)
                    except ValueError:
                        unknown_task_ids.append((i, b32_task_id))
                        continue

                    task = pimpy_repository.find_task_in_group_by_id(task_id, group.id)
                    if not task:
                        unknown_task_ids.append((i, b32_task_id))
                    else:
                        done_list.append((i, task))

            # Mark a comma separated list as removed.
            for task_id_list in REMOVE_REGEX.findall(line):
                for b32_task_id in task_id_list.strip().split(","):
                    b32_task_id = b32_task_id.strip()
                    try:
                        task_id = b32.decode(b32_task_id)
                    except ValueError:
                        unknown_task_ids.append((i, b32_task_id))
                        continue

                    task = pimpy_repository.find_task_in_group_by_id(task_id, group.id)
                    if not task:
                        unknown_task_ids.append((i, b32_task_id))
                    else:
                        remove_list.append((i, task))

        # Catch invalid user.
        except ValidationException:
            unknown_user.append((i, line))

        try:
            # Search for Meeting
            meeting = find_meeting_in_line(line)

            if meeting is not None:
                meeting_list.append(meeting)

        except ValidationException:
            invalid_meetings.append((i, line))

    if missing_colon_lines or unknown_task_ids or unknown_user or invalid_meetings:
        raise InvalidMinuteException(
            missing_colon_lines, unknown_task_ids, unknown_user, invalid_meetings
        )

    return task_list, done_list, remove_list, meeting_list


MeetingParseResponse = Optional[Tuple[str, datetime, timedelta]]


def find_meeting_in_line(line) -> MeetingParseResponse:
    """
    Parse minutes in to meetings

    Format: MEETING day-month-year hour:minute(optional: length in hours) title
    For example: MEETING 06-08-2001 09:00(8,5) via's birthday!
    for length: the floats can be with a . or a ,

    Length in hours has a maximum of 99 hours

    Leading zero's aren't required (leading zero is required at the minutes)
    """

    match = MEETING_REGEX.search(line)

    if match is None:
        return None

    if match["title"] is None:
        raise ValidationException("No title")

    if match["date"] and match["time"]:
        start = datetime.strptime(f"{match['date']} {match['time']}", "%d-%m-%Y %H:%M")
        start = pytz.timezone("Europe/Amsterdam").localize(start)
    else:
        raise ValidationException("No start time")

    if match["length"]:
        hours = float(match["length"].replace(",", "."))
    else:
        hours = 1

    return match["title"], start, timedelta(hours=hours)
