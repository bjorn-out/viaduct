import logging
from typing import Dict, List, Tuple, TypedDict

from app.exceptions.base import AuthorizationException
from app.models.group import Group
from app.models.user import User
from app.repository import role_repository
from app.roles import Roles

_logger = logging.getLogger(__name__)


class RoleDetails(TypedDict):
    """
    :has_role True if the user has the role
    :needs_tfa True if :has_role would be True when the user enabled 2FA.
    """

    has_role: bool
    needs_tfa: bool


def check_user_has_role(user, *roles: Roles):
    if not user_has_role(user, *roles):
        names = [role.name for role in roles]
        raise AuthorizationException(
            f"User does not have {', '.join(names)} permission."
        )


def user_has_role(user: User, *roles: Roles):
    """
    Checks if a user has (a) certain role(s).

    If the user has a role that requires tfa, but the user does not have this
    enabled, false will be returned.

    :param user:
    :param roles:
    :return:
    """
    return user_has_role_and_tfa(user, *roles)[0]


def user_has_role_and_tfa(user: User, *roles: Roles) -> Tuple[bool, bool]:
    """
    Checks if a user has (a) certain role(s).

    :param user:
    :param roles:
    :return: a tuple containing two flags indicating
        - If the user is authenticated for the roll.
        - If the user would have been authenticated if they had tfa enabled.

    """
    if not user or not user.is_authenticated or user.disabled:
        return False, False

    user_roles = role_repository.load_user_roles(user.id)

    if not all(role in user_roles for role in roles):
        return False, False

    if any(role.tfa_required for role in roles) and not user.tfa_enabled:
        return False, True

    return True, False


def user_role_summary(user) -> Dict[Roles, RoleDetails]:
    roles = set(role_repository.load_user_roles(user.id))

    return {
        role: RoleDetails(
            has_role=role in roles
            and not (role.tfa_required and not bool(user.tfa_enabled)),
            needs_tfa=role.tfa_required and not user.tfa_enabled,
        )
        for role in Roles
    }


def user_get_roles(user: User) -> List[Roles]:
    roles = user_role_summary(user)
    return [role for role, details in roles.items() if details["has_role"]]


def user_has_any_role(user: User, *roles):
    if not user or not user.is_authenticated or user.disabled:
        return False

    user_roles = role_repository.load_user_roles(user.id)
    return any(role in user_roles for role in roles)


def user_roles_with_groups(user: User) -> Dict[Roles, List[Group]]:
    return role_repository.load_user_roles_with_groups(user_id=user.id)


def user_groups_with_roles(user: User) -> Dict[Group, List[Roles]]:
    return role_repository.load_user_groups_with_roles(user_id=user.id)


def user_needs_two_factor_authentication(user: User) -> bool:
    """Checks if the user has a permission that requires tfa"""

    roles = role_repository.load_user_roles(user.id)

    return any(role.tfa_required for role in roles)


def find_all_roles_by_group_id(group_id):
    return role_repository.find_all_roles_by_group_id(group_id)


def set_roles_for_group(group_id, new_roles):
    """Update a groups roles, uses set theory to determine what changed."""
    current_roles = set(find_all_roles_by_group_id(group_id))
    new_roles = set(new_roles)

    removed_roles = current_roles - new_roles
    added_roles = new_roles - current_roles

    if removed_roles:
        role_repository.delete_roles_by_group(group_id, removed_roles)
    if added_roles:
        role_repository.insert_roles_by_group(group_id, added_roles)
