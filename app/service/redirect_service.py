from typing import List, Optional, Type, Union

from flask_sqlalchemy import Pagination

from app.api.schema import PageSearchParameters
from app.exceptions.base import ResourceNotFoundException, DuplicateResourceException
from app.models.redirect import Redirect
from app.repository import redirect_repository, model_service


def create_redirection(fro: str, to: str) -> Type[Redirect]:
    if redirect_repository.redirect_exists(fro=fro):
        raise DuplicateResourceException("redirect", fro)

    r = Redirect(fro=fro, to=to)

    return model_service.save(r)


def get_all() -> List[Redirect]:
    return model_service.get_all(Redirect, Redirect.fro)


def paginated_search_all_redirects(pagination: PageSearchParameters) -> Pagination:
    return redirect_repository.paginated_search_all_groups(pagination)


def find_by_path(path: str) -> Optional[Redirect]:
    return redirect_repository.find_by_path(path)


def get_by_path(path: str) -> Redirect:
    redirect = redirect_repository.find_by_path(path)
    if redirect is None:
        raise ResourceNotFoundException("redirect", path)
    return redirect


def upsert_redirection(fro: str, to: str) -> Union[Redirect, Type[Redirect]]:
    redirect = find_by_path(fro)

    if redirect:
        return redirect_repository.edit_redirection(redirect, fro, to)

    return create_redirection(fro, to)
