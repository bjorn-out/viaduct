import json
from typing import Optional

from app.models.setting_model import Setting
from app.repository import setting_repository

SETTING_KEY = "SITE_BANNER"
DEFAULT_BACKGROUND = "#304ba3"


class Banner:
    def __init__(self, message_nl, message_en, background=DEFAULT_BACKGROUND):
        self.message_nl = message_nl
        self.message_en = message_en
        self.background = background


def find_banner() -> Optional[Banner]:
    s = setting_repository.find_by_key(SETTING_KEY)
    if not s:
        return None
    dict = json.loads(s.value)

    nl = dict.get("message_nl", "")
    en = dict.get("message_en", "")
    background = dict.get("background", DEFAULT_BACKGROUND)

    return Banner(nl, en, background)


def set_banner(banner: Banner):
    s = setting_repository.find_by_key(SETTING_KEY)
    if not s:
        s = Setting()
        s.key = SETTING_KEY
    s.value = json.dumps(
        {
            "message_nl": banner.message_nl,
            "message_en": banner.message_en,
            "background": banner.background,
        }
    )
    setting_repository.save(s)
