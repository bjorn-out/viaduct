import logging
import json
from functools import wraps
from typing import Dict, List, NamedTuple

import requests

from app import app
from app.exceptions.base import ResourceNotFoundException
from app.models.user import User
from app.repository import user_repository
from app.service import (
    mailinglist_service,
    user_mailing_service,
    user_service,
    education_service,
    setting_service,
)

_logger = logging.getLogger(__name__)


def copernica_dict_for_user(user: User) -> Dict[str, str]:
    """
    Create a POST body for the copernica API

    Uses the following page for formatting dates.
    https://www.copernica.com/en/documentation/database-fields-and-collections
    """
    if not user.educations:
        education_str = "Other"
    else:
        # Most "important" programme is returned first.
        programmes = user_service.get_user_sorted_study_programmes(user)

        education = programmes[0] if len(programmes) > 0 else None
        if education:
            education_str = education_service.get_education_name(education, "en")
        else:
            education_str = "Other"

    data = {
        "Emailadres": user.email,
        "Voornaam": user.first_name,
        "Achternaam": user.last_name,
        "Studie": education_str,
        "Studienummer": user.student_id,
        "Lid": "Ja" if user.has_paid else "Nee",
        "Alumnus": "Ja" if user.alumnus else "Nee",
        "VVV": "Ja" if user.favourer else "Nee",
        "Geboortedatum": (
            user.birth_date.strftime("%Y-%m-%d") if user.birth_date else "0000-00-00"
        ),
        "Taal": user.locale,
        "WebsiteID": str(user.id),
    }

    subscribed_mailinglists = set(
        user_mailing_service.get_user_subscribed_mailing_lists(user)
    )

    for mailing_list in mailinglist_service.get_all_mailinglists():

        subscribed = mailing_list in subscribed_mailinglists
        if mailing_list.members_only:
            subscribed = subscribed and user.has_paid  # type: ignore

        data[mailing_list.copernica_column_name] = "Ja" if subscribed else "Nee"

    return data


def handle_webhook(data: Dict[str, str]) -> bool:
    try:
        if data["type"] != "update":
            return False

        if data["database"] != app.config["COPERNICA_DATABASE_ID"]:
            return False

        profile_id = int(data["profile"])
        user = user_service.find_by_copernica_id(profile_id)
        if not user:
            return False

        old_user_mailing_lists = frozenset(user.mailinglists.all())
        new_user_mailing_lists = set(user.mailinglists.all())

        for mailinglist in mailinglist_service.get_all_mailinglists():
            param_name = f"parameters[{mailinglist.copernica_column_name}]"

            value = data.get(param_name)
            if not value:
                continue

            if value == "Ja":
                new_user_mailing_lists.add(mailinglist)
            else:
                # Regard anything else as no, but log weird values
                if value != "Nee":
                    _logger.error(
                        "Received invalid value for "
                        f"Copernica mailing list column: {value}"
                    )

                if mailinglist in new_user_mailing_lists:
                    new_user_mailing_lists.remove(mailinglist)

        if new_user_mailing_lists != old_user_mailing_lists:
            # Unfortunately we cannot detect whether the webhook
            # call was caused by our own API call. Therefore we don't
            # execute another API call when updating the mailing lists
            # to prevent an endless webhook/API call loop.
            user_mailing_service.set_mailing_list_subscriptions(
                user, new_user_mailing_lists, update_copernica=False
            )

        return True

    except (KeyError, ValueError):
        _logger.exception(f"Received invalid content for Copernica webhook: {data}")

        return False


def copernica_enabled(default_callable):
    def decorator(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            enabled = app.config["COPERNICA_ENABLED"]

            _logger.info("COPERNICA_ENABLED=%s", enabled)

            if not enabled:
                return default_callable()
            else:
                return f(*args, **kwargs)

        return wrapped

    return decorator


@copernica_enabled(lambda: None)
def send_welcome_mail(user: User):
    url = "https://api.copernica.com/v2/publisher/emailing?access_token={}"
    url = url.format(app.config["COPERNICA_API_KEY"])

    if user.locale == "en":
        document = app.config["COPERNICA_WELCOME_MAIL_EN_DOC_ID"]
    else:
        document = app.config["COPERNICA_WELCOME_MAIL_NL_DOC_ID"]

    r = requests.post(
        url,
        data={
            "target": user.copernica_id,
            "targettype": "profile",
            "document": document,
            "settings": {"start": False, "iterations": 1},
        },
    )

    if r.status_code != 201:
        _logger.error(
            f"Could send delete welcome mail to profile {user.copernica_id}"
            f" from Copernica. status_code={r.status_code}, data={r.json()}"
        )


class CopernicaDBField(NamedTuple):
    ID: int
    name: str
    type: str
    value: str


class CopernicaProfileInfo(NamedTuple):
    ID: int
    database_id: int
    secret: str
    fields: Dict[str, str]


def copernica_integration_enabled():
    return app.config["COPERNICA_ENABLED"]


@copernica_enabled(list)
def get_database_fields() -> List[CopernicaDBField]:
    url = "https://api.copernica.com/database/{}/fields?access_token={}"
    url = url.format(
        app.config["COPERNICA_DATABASE_ID"], app.config["COPERNICA_API_KEY"]
    )

    r = requests.get(url)
    if r.status_code != 200:
        _logger.error(
            "Got invalid response from Copernica. "
            f"status_code={r.status_code}, data={r.json()}"
        )
        return []

    return [
        CopernicaDBField(
            ID=int(field["ID"]),
            name=field["name"],
            type=field["type"],
            value=field["value"],
        )
        for field in r.json()["data"]
    ]


def get_disabling_selections() -> Dict[str, int]:
    try:
        s = setting_service.get_setting_by_key("COPERNICA_DISABLING_SELECTIONS")
        return json.loads(s.value)
    except ResourceNotFoundException:
        _logger.warning("COPERNICA_DISABLING_SELECTIONS setting not found.")
        return {}


@copernica_enabled(list)
def get_selection_profiles(selection_id: int):
    url = "https://api.copernica.com/v2/view/{}/profileids?access_token={}"
    url = url.format(selection_id, app.config["COPERNICA_API_KEY"])

    r = requests.get(url)
    if r.status_code != 200:
        _logger.error(
            "Got invalid response from Copernica. "
            f"status_code={r.status_code}, data={r.json()}"
        )
        return []
    return list(map(int, r.json()))


@copernica_enabled(lambda: None)
def update_profile(user: User) -> None:
    data = copernica_dict_for_user(user)

    if not user.copernica_id:
        _logger.warning("%s missing copernica_id", user)
        return

    url = (
        "https://api.copernica.com/database/{}/profiles?"
        "fields[]=ID%3D%3D{}&access_token={}"
    )
    url = url.format(
        app.config["COPERNICA_DATABASE_ID"],
        user.copernica_id,
        app.config["COPERNICA_API_KEY"],
    )
    requests.put(url, data)


@copernica_enabled(lambda: None)
def create_profile(user: User) -> None:
    data = copernica_dict_for_user(user)

    if user.copernica_id:
        _logger.error("%s has copernica_id", user)
        return

    url = "https://api.copernica.com/database/{}/profiles?access_token={}"
    url = url.format(
        app.config["COPERNICA_DATABASE_ID"], app.config["COPERNICA_API_KEY"]
    )
    r = requests.post(url, data)

    user.copernica_id = int(r.headers["X-Created"])
    user_repository.save(user)


@copernica_enabled(lambda: None)
def get_profile_info(profile_id: int):
    url = "https://api.copernica.com/profile/{}/?access_token={}"
    url = url.format(profile_id, app.config["COPERNICA_API_KEY"])

    r = requests.get(url)
    data = r.json()

    if r.status_code != 200:
        if data["error"]["message"] != "No entity with supplied ID":
            _logger.error(
                "Got invalid response from Copernica. "
                f"status_code={r.status_code}, data={data}"
            )

        return None

    return CopernicaProfileInfo(
        ID=int(data["ID"]),
        database_id=data["database"],
        secret=data["secret"],
        fields=data["fields"],
    )


@copernica_enabled(lambda: None)
def delete_profile(profile_id: int):
    url = "https://api.copernica.com/profile/{}/?access_token={}"
    url = url.format(profile_id, app.config["COPERNICA_API_KEY"])

    r = requests.delete(url)
    if r.status_code != 200:
        _logger.error(
            f"Could not delete profile {profile_id} from Copernica."
            f"status_code={r.status_code}, data={r.json()}"
        )
