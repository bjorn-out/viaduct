from typing import List, Tuple

from app.enums import ProgrammeType
from app.exceptions.base import ValidationException
from app.models.education import Education
from app.models.mollie import Transaction, TransactionMembership
from app.models.user import User
from app.repository import user_repository
from app.service import copernica_service, mollie_service, user_service
from app.task import copernica
from config import Config

MEMBERSHIP_NORMAL_PRICE = -1
MEMBERSHIP_MASTER_PRICE = -1


@Config.post_load
def _membership_config(config: Config):
    global MEMBERSHIP_NORMAL_PRICE, MEMBERSHIP_MASTER_PRICE

    MEMBERSHIP_NORMAL_PRICE = config.MEMBERSHIP_NORMAL_PRICE
    MEMBERSHIP_MASTER_PRICE = config.MEMBERSHIP_MASTER_PRICE


def can_pay_membership(user: User) -> bool:
    educations: List[Education] = user.educations.all()
    return is_user_valid_for_auto_confirmation(user) and is_any_study_from_via(
        educations
    )


def is_user_valid_for_auto_confirmation(user: User) -> bool:
    return (
        not user.is_anonymous
        and user.is_authenticated
        and not user.has_paid
        and user.is_active
        and not user.alumnus
        and user.student_id_confirmed
    )


def is_any_study_from_via(educations: List[Education]) -> bool:
    # TODO: this relies on that the studies are up-to-date
    # they are almost always when the user signs up when the account was just
    # created, but there might have to be a task that repeatedly updates this.
    for edu in educations:  # type: Education
        if edu.is_via_programme:
            return True

    return False


def get_membership_price_from_via_studies(educations: List[Education]) -> int:
    # Get the membership price for the given list of studies.
    # Calling this without checking if any study is from via will result in a
    # ValidationException.

    for edu in educations:
        if edu.is_via_programme and (
            edu.programme_type == ProgrammeType.MASTER
            or edu.programme_type == ProgrammeType.PRE_MASTER
        ):
            return MEMBERSHIP_MASTER_PRICE

    for edu in educations:
        if edu.is_via_programme:
            return MEMBERSHIP_NORMAL_PRICE

    raise ValidationException("None of the educations are from via")


def confirm_free_membership(user: User):
    educations: List[Education] = user.educations.all()
    if (
        not is_user_valid_for_auto_confirmation(user)
        or not is_any_study_from_via(educations)
        or get_membership_price_from_via_studies(educations) != 0
    ):
        raise ValidationException("Not eligible for free membership")

    set_membership_status(user, True)


def create_membership_transaction(user: User) -> Tuple[Transaction, str]:
    if not can_pay_membership(user):
        raise ValidationException("Cannot pay membership")

    callback = TransactionMembership.from_user(user)

    description = "{} ({}), {}".format(user.name, user.student_id, "via membership fee")

    transaction, checkout_url = mollie_service.create_transaction(
        amount=get_membership_price_from_via_studies(user.educations),
        description=description,
        user=user,
        callback=callback,
    )

    return transaction, checkout_url


def on_membership_paid(transaction: TransactionMembership):
    # note: this method may be called for the webhook request, a user or a
    # manual check for the payment.
    # the user in the callback may not be the current user,
    # for example when the user session expired when the transaction was
    # completed and the browser was redirected to the user.
    # it completely depends on what payment was completed and notified with
    # the callback.
    user = user_service.find_by_id(transaction.user_id)
    set_membership_status(user, True)


def set_membership_status(user, has_paid: bool = True):
    # No-op when status is the same.
    if user.has_paid == has_paid:
        return

    user.has_paid = has_paid

    user_repository.save(user)

    copernica.update_user.delay(user.id)
    if has_paid:
        copernica_service.send_welcome_mail(user)
