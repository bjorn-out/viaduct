from flask_restful import Resource
from marshmallow import fields
from app.api.schema import RestSchema
from app.oauth_scopes import Scopes
from app.service import declaration_service
from app.decorators import require_oauth
from app.models.user import User


class UserDeclarationsSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    created = fields.String(required=True)
    modified = fields.String(required=True)
    committee = fields.String(required=True)
    amount = fields.Float(required=True)
    reason = fields.String(required=True)
    user_id = fields.Integer(required=True)


class UserDeclarationsResource(Resource):
    schema = UserDeclarationsSchema(many=True)

    @require_oauth(Scopes.declaration)
    def get(self, user: User):
        declarations = declaration_service.get_all_declarations_by_user_id(user.id)
        return self.schema.dump(declarations)
