from flask_restful import Resource
from marshmallow import Schema, fields

from app.api.schema import EnumField
from app.decorators import json_schema, require_oauth, require_role
from app.models.group import Group
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import role_service


class RoleListSchema(Schema):
    roles = fields.List(EnumField(Roles))
    options = fields.List(EnumField(Roles), dump_only=True)


class GroupRoleSchema(Resource):
    schema = RoleListSchema()

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_PERMISSIONS)
    def get(self, group: Group):
        return self.schema.dump(
            {
                "roles": role_service.find_all_roles_by_group_id(group.id),
                "options": list(Roles),
            }
        )

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_PERMISSIONS)
    @json_schema(schema)
    def put(self, data, group: Group):
        role_service.set_roles_for_group(group.id, data["roles"])
        return self.schema.dump(
            {
                "roles": role_service.find_all_roles_by_group_id(group.id),
                "options": list(Roles),
            }
        )
