from http import HTTPStatus

from authlib.integrations.flask_oauth2 import current_token
from flask import Response
from flask_restful import Resource
from marshmallow import fields

from app.api.schema import (
    AutoMultilangStringField,
    PageSearchParameters,
    PaginatedResponseSchema,
    PaginatedSearchSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import news_service


class NewsSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    title = AutoMultilangStringField(required=True)
    content = AutoMultilangStringField(required=True)
    publish_date = fields.Date(required=True)


class NewsResource(Resource):
    schema = NewsSchema()

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    def get(self, news):
        return self.schema.dump(news)

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    @json_schema(schema)
    def put(self, data, news):
        news = news_service.update_news_item(news, data)
        return self.schema.dump(news)

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    def delete(self, news):
        news_service.delete_news(news)
        return Response(status=HTTPStatus.NO_CONTENT)


class NewsListResource(Resource):
    schema = NewsSchema()
    schema_get = PaginatedResponseSchema(NewsSchema(many=True))
    schema_search = PaginatedSearchSchema()

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    @query_parameter_schema(schema_search)
    def get(self, pagination: PageSearchParameters):
        return self.schema_get.dump(news_service.paginated_search_all_news(pagination))

    @require_oauth(Scopes.newsletter)
    @require_role(Roles.NEWS_WRITE)
    @json_schema(schema)
    def post(self, data):
        news = news_service.create_news_item(data, current_token.user)
        return self.schema.dump(news), HTTPStatus.CREATED
