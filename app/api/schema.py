from typing import Dict, List, NamedTuple, Tuple, Type, TypeVar, TypedDict

import pydantic
from flask.views import MethodView
from marshmallow import (
    RAISE,
    Schema,
    SchemaOpts,
    ValidationError,
    fields,
    post_load,
    pre_dump,
    utils as marshmallow_utils,
    validates,
)
from marshmallow.fields import Field
from marshmallow.utils import missing as missing_

from app.exceptions.base import ResourceNotFoundException
from app.models.company_job import CONTRACT_OF_SERVICE_TYPES


class RestSchemaOpts(SchemaOpts):
    """
    Overwrite the defaults for the meta class.

    Raise ValidationError if there are any unknown fields in deserialization.
    Always maintain field ordering of serialized output.
    """

    def __init__(self, meta, ordered):
        super().__init__(meta, ordered=ordered)
        self.unknown = getattr(meta, "unknown", RAISE)
        self.ordered = getattr(meta, "ordered", True)


class RestSchema(Schema):
    OPTIONS_CLASS = RestSchemaOpts


class PaginatedResponseSchema(RestSchema):
    page = fields.Integer()
    page_size = fields.Integer()
    page_count = fields.Integer()
    total = fields.Integer()

    data = fields.Method("get_data", many=True)

    def __init__(self, subschema: Schema, *args, **kwargs) -> None:
        super(PaginatedResponseSchema, self).__init__(*args, **kwargs)
        self.subschema = subschema

    @pre_dump
    def unpack_pagination(self, pagination_obj, **kwargs):
        return {
            "page": pagination_obj.page,
            "page_size": pagination_obj.per_page,
            "page_count": pagination_obj.pages,
            "total": pagination_obj.total,
            "data": pagination_obj.items,
        }

    def get_data(self, obj):
        return self.subschema.dump(obj["data"])


class PageSearchParameters(NamedTuple):
    search: str
    page: int
    limit: int


class PaginatedSearchSchema(RestSchema):
    search = fields.Str(missing="")
    page = fields.Integer(missing=1)
    limit = fields.Integer(missing=10)

    @validates("page")
    def validate_page(self, page):
        if page <= 0:
            raise ValidationError("Page numbering starts at 1")

    @validates("limit")
    def validate_limit(self, limit):
        if limit > 50:
            raise ValidationError("Maximum page size is 50")
        if limit < 1:
            raise ValidationError("Minimum page size is 1")

    @post_load
    def make_pagination(self, data, **kwargs):
        return {"pagination": PageSearchParameters(**data)}


class PageCourseSearchParameters(PageSearchParameters):
    dn_course_ids: List[int]
    user_educations: List[Tuple[int]]

    # Extending a NamedTuple class and thus using __new__().
    def __new__(cls, search="", page=1, limit=15, dn_course_ids="", user_educations=""):
        self = super(PageCourseSearchParameters, cls).__new__(cls, search, page, limit)

        if dn_course_ids == "":
            self.dn_course_ids = []
        else:
            self.dn_course_ids = list(map(int, dn_course_ids.split(",")))

        if user_educations == "":
            self.user_educations = []
        else:
            self.user_educations = list(map(int, user_educations.split(",")))
            # educations are always send in "<edu_id>,<year>,<edu_id>,<year>,..."
            self.user_educations = [
                (self.user_educations[i], self.user_educations[i + 1])
                for i in range(0, len(self.user_educations), 2)
            ]

        return self


class PaginatedCourseSearchSchema(PaginatedSearchSchema):
    dn_course_ids = fields.String()
    user_educations = fields.String()

    @post_load
    def make_pagination(self, data, **kwargs):
        return {"pagination": PageCourseSearchParameters(**data)}


class PageJobSearchParameters(PageSearchParameters):
    # Extending a NamedTuple class and thus using __new__().
    def __new__(cls, search, page, limit, contract_of_service):
        self = super(PageJobSearchParameters, cls).__new__(cls, search, page, limit)

        if contract_of_service == "":
            self.contract_of_service = CONTRACT_OF_SERVICE_TYPES
        else:
            self.contract_of_service = [
                t
                for t in contract_of_service.split(",")
                if t in CONTRACT_OF_SERVICE_TYPES
            ]

        return self

    contract_of_service: List[str]


class PaginatedJobSearchSchema(PaginatedSearchSchema):
    contract_of_service = fields.String()

    @post_load
    def make_pagination(self, data, **kwargs):
        return {"pagination": PageJobSearchParameters(**data)}


class MultilangStringField(Field):
    default_error_messages = {
        "invalid": "Not a valid dict of languages.",
        "invalid_utf8": "Not a valid utf-8 string.",
        "missing_data": "Not all languages supplied",
    }

    langs = ["nl", "en"]

    def _serialize(self, value, attr, obj, **kwargs):
        return self._verify_dict(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return self._verify_dict(value)

    def _verify_dict(self, value):
        if not isinstance(value, dict):
            raise self.make_error("invalid")

        r = {}
        for lang in self.langs:
            if lang not in value or not isinstance(value[lang], str):
                r[lang] = ""
            else:
                try:
                    r[lang] = marshmallow_utils.ensure_text_type(value[lang])
                except UnicodeDecodeError as e:
                    raise self.make_error("invalid_utf8") from e
        return r


class AutoMultilangStringField(MultilangStringField):
    def __init__(self, *args, attribute_format="{lang}_{attr}", **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.attribute_format = attribute_format

    def get_value(self, obj, attr, accessor=None, default=missing_):
        return {
            lang: super(AutoMultilangStringField, self).get_value(
                obj,
                self.attribute_format.format(attr=attr, lang=lang),
                accessor,
                default,
            )
            for lang in self.langs
        }


class MultilangStringDict(TypedDict):
    en: str
    nl: str


class EnumField(Field):
    """Based on https://github.com/justanr/marshmallow_enum"""

    default_error_messages = {
        "by_name": "Invalid enum member {input}",
        "must_be_string": "Enum name must be string",
    }

    def __init__(self, enum, by_value=False, error="", *args, **kwargs):
        self.enum = enum
        self.by_value = by_value
        self.error = error

        super(EnumField, self).__init__(*args, **kwargs)

    def _serialize(self, value, attr, obj):
        if value is None:
            return None
        return value.name

    def _deserialize(self, value, attr, data, **kwargs):
        if value is None:
            return None
        return self._deserialize_by_name(value, attr, data)

    def _deserialize_by_name(self, value, attr, data):
        if not isinstance(value, str):
            self.fail("must_be_string", input=value, name=value)

        try:
            return getattr(self.enum, value)
        except AttributeError:
            self.fail("by_name", input=value, name=value)

    def fail(self, key, **kwargs):
        kwargs["values"] = ", ".join([str(mem.value) for mem in self.enum])
        kwargs["names"] = ", ".join([mem.name for mem in self.enum])

        if self.error:
            if self.by_value:
                kwargs["choices"] = kwargs["values"]
            else:
                kwargs["choices"] = kwargs["names"]
            msg = self.error.format(**kwargs)
            raise ValidationError(msg)
        else:
            super(EnumField, self).fail(key, **kwargs)


class TextAreaStr(pydantic.ConstrainedStr):
    @classmethod
    def __modify_schema__(cls, field_schema):
        super().__modify_schema__(field_schema)
        pydantic.types.update_not_none(field_schema, format="textarea")


class UserIdInt(pydantic.ConstrainedInt):
    gt = 0

    @classmethod
    def __modify_schema__(cls, field_schema):
        super().__modify_schema__(field_schema)
        pydantic.types.update_not_none(field_schema, format="user")


S = TypeVar("S", bound=pydantic.BaseModel)


class SchemaRegistery:
    def __init__(self):
        self._registry: Dict[str, Type[S]] = {}

    def get(self, item):
        return self._registry.get(item)

    def register(self, cls: Type[S]) -> Type[S]:
        schema_name = cls.__name__
        if schema_name in self._registry:
            existing_schema = self._registry[schema_name]
            raise ValueError(
                f"Schema {schema_name} already registered: "
                f"{existing_schema.__qualname__}"
            )
        self._registry[schema_name] = cls
        return cls


schema_registry = SchemaRegistery()


class SchemaResource(MethodView):
    def get(self, schema_name: str):
        schema = schema_registry.get(schema_name)
        if not schema:
            raise ResourceNotFoundException("schema", schema_name)
        return schema.schema_json(), 200
