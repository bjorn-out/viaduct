from http import HTTPStatus
from typing import Tuple

from authlib.integrations.flask_oauth2 import current_token
from authlib.oauth2.rfc6749 import MissingAuthorizationError
from flask_restful import Resource, abort
from marshmallow import ValidationError, fields, validates_schema
from marshmallow.validate import OneOf

from app.api.company.company import CompanyModuleSchemaMixin
from app.api.schema import (
    PageJobSearchParameters,
    PaginatedJobSearchSchema,
    PaginatedResponseSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.models.company import Company
from app.models.company_job import CONTRACT_OF_SERVICE_TYPES
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service, role_service


class CompanyJobSchema(RestSchema, CompanyModuleSchemaMixin):
    title_nl = fields.String()
    title_en = fields.String()

    contact_name = fields.String(required=True)
    contact_email = fields.Email(required=True)
    contact_address = fields.String(required=True)
    contact_city = fields.String(required=True)

    contract_of_service = fields.String(
        required=True, validate=OneOf(CONTRACT_OF_SERVICE_TYPES)
    )
    website = fields.URL(required=True)
    phone = fields.String(required=True)

    description_nl = fields.String()
    description_en = fields.String()

    @classmethod
    def get_list_schema(cls):
        return cls(
            many=True,
            only=(
                "id",
                "title_nl",
                "title_en",
                "company_id",
                "start_date",
                "end_date",
                "active",
                "contract_of_service",
                "description_nl",
                "description_en",
            ),
        )

    @validates_schema
    def validate_languages(self, data, **_):  # pylint: disable=no-self-use
        if not data.get("title_nl") and not data.get("title_en"):
            raise ValidationError(
                "Dutch or English title required.", ["title_nl", "title_en"]
            )

        if bool(data.get("title_nl")) != bool(data.get("description_nl")):
            raise ValidationError(
                "Dutch title requires dutch description and vice versa.",
                ["title_nl", "description_nl"],
            )
        if bool(data.get("title_en")) != bool(data.get("description_en")):
            raise ValidationError(
                "English title requires english description and vice versa.",
                ["title_en", "description_en"],
            )


class CompanyJobListSchema(RestSchema):
    id = fields.Integer()
    title_nl = fields.String()
    title_en = fields.String()
    company_id = fields.Integer()
    company_name = fields.String()
    contract_of_service = fields.String()
    description_nl = fields.String()
    description_en = fields.String()
    active = fields.Boolean()


class JobResource(Resource):
    schema = CompanyJobSchema()

    @require_oauth(optional=True)
    def get(self, job_id: int):
        is_admin = False

        try:
            if current_token and current_token.user:
                is_admin = role_service.user_has_role(
                    current_token.user, Roles.COMPANY_WRITE
                )
        except MissingAuthorizationError:
            # Not admin, show only when admin.
            pass

        job = company_service.get_job_by_id(job_id)
        if not job.active and not is_admin:
            abort(403)

        return self.schema.dump(job)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def put(self, job_update: dict, job_id: int):
        job = company_service.edit_company_job(job_id, **job_update)
        return self.schema.dump(job)


class JobListResource(Resource):
    """Public API, so no scope or role required."""

    schema_get = PaginatedResponseSchema(CompanyJobListSchema(many=True))
    schema_search = PaginatedJobSearchSchema()

    @query_parameter_schema(schema_search)
    def get(self, pagination: PageJobSearchParameters):

        jobs = company_service.paginated_search_all_jobs(pagination)
        return self.schema_get.dump(jobs)


class JobContractsOfServices(Resource):
    def get(self):

        return {
            "contract_of_service": [
                j for j, in company_service.find_all_contract_of_services(True)
            ]
        }


class CompanyJobListResource(Resource):
    schema_get = CompanyJobSchema.get_list_schema()
    schema_post = CompanyJobSchema()

    @require_oauth(Scopes.company, optional=True)
    def get(self, company: Company) -> Tuple[dict, int]:
        is_admin = False

        if current_token:
            is_admin = role_service.user_has_role(
                current_token.user, Roles.COMPANY_WRITE
            )

        # If the user is not an administrator, filter the inactive companies.
        if is_admin:
            jobs = company_service.find_company_jobs_by_company_id(company.id)
        else:
            jobs = company_service.find_company_jobs_by_company_id(
                company_id=company.id, filter_inactive=True
            )
        return self.schema_get.dump(jobs), HTTPStatus.OK

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema_post)
    def post(self, job_details: dict, company: Company) -> Tuple[dict, int]:
        job = company_service.create_company_job(company_id=company.id, **job_details)
        return self.schema_post.dump(job), HTTPStatus.CREATED
