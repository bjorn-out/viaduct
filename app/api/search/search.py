from authlib.integrations.flask_oauth2 import current_token
from flask_restful import Resource
from marshmallow import Schema, fields

from app.decorators import query_parameter_schema, require_oauth, require_role
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service.search_service import execute_search_request, get_allowed_indices


class SearchSchema(Schema):
    query = fields.String(required=True)
    index = fields.String(required=False, missing="viaduct")
    limit = fields.Integer(required=False, missing=10)
    offset = fields.Integer(required=False, missing=0)


class SearchResource(Resource):
    schema = SearchSchema()

    @require_oauth(Scopes.search)
    @require_role(Roles.SEARCH_READ)
    @query_parameter_schema(schema)
    def get(self, query: str, index: str, limit: int, offset: int):
        user = current_token.user
        return execute_search_request(
            user=user, query=query, index=index, limit=limit, offset=offset
        )


class SearchIndexResource(Resource):
    @require_oauth(Scopes.search)
    @require_role(Roles.SEARCH_READ)
    def get(self):
        user = current_token.user
        return {"indices": get_allowed_indices(user)}
