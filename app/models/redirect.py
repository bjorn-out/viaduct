from sqlalchemy import Column, String

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class Redirect(BaseEntity):
    __tablename__ = "redirect"

    fro: str = Column(String(200), unique=True, nullable=False)
    to: str = Column(String(200), nullable=False)
