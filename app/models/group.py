# To prevent circular dependencies with page and committee,
# CommitteeRevison must be imported inside functions
import logging
from datetime import datetime, timezone
from typing import Any, List, TYPE_CHECKING

from sqlalchemy import Column, DateTime, Enum, Integer, String
from sqlalchemy.orm import backref, relationship
from sqlalchemy.schema import ForeignKey

from app import db
from app.exceptions.base import ResourceNotFoundException
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.user import User  # noqa
    from app.models.role_model import GroupRole  # noqa


_logger = logging.getLogger(__name__)


@mapper_registry.mapped
class UserGroup:
    __tablename__ = "user_group"
    user_id: int = Column(
        Integer, ForeignKey("user.id"), primary_key=True, nullable=False
    )
    group_id: int = Column(
        Integer, ForeignKey("group.id"), primary_key=True, nullable=False
    )
    created: datetime = Column(
        DateTime(timezone=True), nullable=False, default=datetime.now
    )


@mapper_registry.mapped
class UserGroupHistory(BaseEntity):
    __tablename__ = "user_group_history"
    deleted = Column(DateTime(timezone=True), nullable=True)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    group_id = Column(Integer, ForeignKey("group.id"), nullable=False)

    @classmethod
    def from_user_group(cls, user_group: UserGroup):
        history = cls()
        history.created = user_group.created
        history.user_id = user_group.user_id
        history.group_id = user_group.group_id
        history.deleted = datetime.now(timezone.utc)
        return history


ILLEGAL_MAILLIST_PREFIXES = ["list-", "coordinator-"]


@mapper_registry.mapped
class Group(BaseEntity):
    __tablename__ = "group"
    prints = ("id", "name")

    name = Column(String(200), unique=True)

    # TODO Remove any: https://github.com/sqlalchemy/sqlalchemy/issues/6229
    users: Any = relationship(
        "User",
        secondary="user_group",
        backref=backref("groups", lazy="joined", order_by="Group.name"),
        lazy="dynamic",
    )

    mailtype = Column(
        Enum("none", "mailinglist", "mailbox", name="group_mailing_type"),
        nullable=False,
    )
    maillist = Column(String(100), unique=True)
    roles: List["GroupRole"] = relationship(
        "GroupRole", back_populates="group", uselist=True
    )

    def __init__(self, name, maillist=None, mailtype=None):
        self.name = name
        if maillist is None:
            self.mailtype = "none"
        else:
            if mailtype not in ("mailinglist", "mailbox"):
                raise ValueError(
                    "When maillist is set, mailtype must be either "
                    "'mailinglist' or 'mailbox'"
                )

            self.maillist = maillist
            self.mailtype = mailtype

    def is_committee(self, id):
        from app.models.committee import CommitteeRevision

        u = db.session.query(CommitteeRevision).filter(CommitteeRevision.group_id == id)
        return db.session.query(u.exists()).first()[0]

    def has_user(self, user):
        if not user:
            return False
        else:
            return self.users.filter(UserGroup.user_id == user.id).count() > 0

    def delete_user(self, user):
        from app.service import google_service

        if self.has_user(user):
            google_service.remove_email_from_google_group(self, user.email)
            if self.is_committee(self.id):
                # Create fake aal group (active alumni list)
                aal_group = Group(name="aal", maillist="aal", mailtype="mailinglist")
                try:
                    google_service.add_email_to_google_group(aal_group, user.email)
                except ResourceNotFoundException:
                    _logger.error("aal@svia.nl not found in Google groups")

            self.users.remove(user)
