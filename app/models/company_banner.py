from sqlalchemy import Column, String

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.company import CompanyModuleMixin


@mapper_registry.mapped
class CompanyBanner(BaseEntity, CompanyModuleMixin):
    __tablename__ = "company_banner"
    website = Column(String(512))
