from functools import wraps

from flask import Blueprint, flash, redirect, render_template, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.models.meeting import Meeting
from app.service import group_service, meeting_service
from app.utils.pagination import Pagination
from app.views.viewmodel.meeting import MeetingViewModel

blueprint = Blueprint("meeting", __name__, url_prefix="/meeting")


def require_access_meeting(f):
    """
    Check whether the user has access to the meeting.

    NOTE: Assumes that the meeting_id is the first parameter in the view
    function.
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        meeting = kwargs.pop("meeting", None)
        if meeting:
            group = group_service.get_by_id(meeting.group_id)

            if group not in group_service.get_groups_for_user(current_user):
                flash(_("You are not part of the given group"))

                redirect(url_for("meeting.view"))
            return f(meeting=meeting, *args, **kwargs)
        return f(*args, **kwargs)

    return wrapper


# TODO change to request.args('page') default in the pagination.
@blueprint.route("/", methods=["GET"])
@blueprint.route("/<string:archive>/", methods=["GET", "POST"])
@blueprint.route("/list/<int:page_nr>/", methods=["GET", "POST"])
@blueprint.route("/<string:archive>/list/<int:page_nr>/", methods=["GET", "POST"])
@login_required
def view(archive: str = None, page_nr: int = 1):
    if archive == "archive":
        meetings = meeting_service.get_by_user(current_user, future=False)
    else:
        meetings = meeting_service.get_by_user(current_user, future=True)

    pagination = Pagination(page_nr, 10, len(meetings), meetings)
    pagination.items = [MeetingViewModel(m) for m in pagination.items]

    return render_template("meeting/view.htm", meetings=pagination, archive=archive)


@blueprint.route("/<meeting:meeting>/", methods=["GET"])
@login_required
@require_access_meeting
def get_meeting(meeting: Meeting):
    view_model = MeetingViewModel(meeting)

    return view_model.render()


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<meeting:meeting>/edit/", methods=["GET"])
@login_required
@require_access_meeting
def edit(meeting: Meeting = None):
    if meeting:
        title = _("Edit meeting")
    else:
        title = _("Create meeting")
    return render_template("vue_content.htm", title=title)
