import logging
import re
from functools import wraps

import werkzeug.exceptions
from flask import flash, jsonify, redirect, render_template, request, session, url_for
from flask_babel import gettext
from flask_login import current_user
from sentry_sdk import last_event_id

from app import app
from app.exceptions.base import (
    ApplicationException,
    AuthorizationException,
    BusinessRuleException,
    DuplicateResourceException,
    ResourceNotFoundException,
    ServiceUnavailableException,
    ValidationException,
)
from app.extensions import login_manager
from app.roles import Roles
from app.service import role_service


@login_manager.unauthorized_handler
def unauthorized():
    # Save the path the user was rejected from.
    session["denied_from"] = request.full_path

    flash(gettext("You must be logged in to view this page."), "danger")
    return redirect(url_for("login.sign_in"))


def add_api_error_handler(f):
    @wraps(f)
    def wrapper(e):
        if request.path.startswith("/api"):
            return handle_api_error(e)

        return f(e)

    return wrapper


def handle_api_error(e):
    if isinstance(e, ApplicationException):
        return jsonify(e.ErrorSchema().dump(e)), e.code

    if not isinstance(e, werkzeug.exceptions.HTTPException):
        e = werkzeug.exceptions.InternalServerError()

    obj = {
        "title": e.name,
        "_message": e.description,
        "code": e.code,
        "type_": "about:blank",
    }

    # Marshmallow errors for validation are placed on the data attribute.
    # TODO Handle validation errors directly.
    if hasattr(e, "data"):
        obj["data"] = e.data

    return jsonify(ApplicationException.ErrorSchema().dump(obj)), e.code


@app.errorhandler(ApplicationException)
@app.errorhandler(BusinessRuleException)
@add_api_error_handler
def default_detailed_exception_handler(e):
    if isinstance(e, AuthorizationException):
        return permission_denied(e)

    logging.error(e, exc_info=True)
    return internal_server_error(e)


@app.errorhandler(400)
@app.errorhandler(ValidationException)
@add_api_error_handler
def bad_request(e):
    return render_template("page/400.htm"), 400


@app.errorhandler(409)
@app.errorhandler(DuplicateResourceException)
@add_api_error_handler
def conflict(e):
    return render_template("page/409.htm"), 409


@app.errorhandler(401)
@app.errorhandler(403)
@add_api_error_handler
def permission_denied(_):
    """When permission denied and not logged in you will be redirected."""

    content = "403, The police has been notified!"
    image = "/static/img/403.jpg"

    if current_user.is_anonymous:
        # Save the path you were rejected from.
        session["denied_from"] = request.full_path
        flash(gettext("You must be logged in to view this page."), "danger")
        return redirect(url_for("login.sign_in"))

    return render_template("page/403.htm", content=content, image=image), 403


@app.errorhandler(500)
@add_api_error_handler
def internal_server_error(_):
    return (
        render_template(
            "page/500.htm",
            sentry_dsn=app.config.get("SENTRY_DSN"),
            sentry_event_id=last_event_id(),
        ),
        500,
    )


@app.errorhandler(404)
@app.errorhandler(ResourceNotFoundException)
@add_api_error_handler
def page_not_found(_):
    # Search for file extension.
    if re.match(r"(?:.*)\.[a-zA-Z]{2,}$", request.path):
        return "", 404

    can_write = role_service.user_has_role(current_user, Roles.PAGE_WRITE)
    return render_template("page/404.htm", can_write=can_write), 404


@app.errorhandler(410)
@add_api_error_handler
def gone(_):
    can_write = role_service.user_has_role(current_user, Roles.PAGE_WRITE)
    return render_template("page/410.htm", can_write=can_write), 410


@app.errorhandler(503)
@app.errorhandler(ServiceUnavailableException)
@add_api_error_handler
def service_unavailable(e):
    return render_template("page/503.htm", e=e), 503
