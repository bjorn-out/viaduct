from typing import Literal

from flask import Blueprint, abort, render_template
from flask_babel import gettext as _

from app.decorators import require_membership, require_role
from app.models.examination import Examination
from app.roles import Roles
from app.views.file import send_file_inline

blueprint = Blueprint("examination", __name__, url_prefix="/examination")


@blueprint.route(
    "/view/<examination:exam>/<any(exam,answers):doc_type>/", methods=["GET"]
)
@require_membership
def view(exam: Examination, doc_type: Literal["exam", "answers"]):
    if doc_type == "exam" and exam.examination_file is not None:
        _file = exam.examination_file
        filename = exam.examination_filename
    elif doc_type == "answers" and exam.answers_file is not None:
        _file = exam.answers_file
        filename = exam.answers_filename
    else:
        return abort(404)

    return send_file_inline(_file, filename)


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<examination:exam>/edit/", methods=["GET"])
@require_role(Roles.EXAMINATION_WRITE)
def edit(exam: Examination = None):
    title = _("New examination")
    if exam:
        title = _("Edit examination")
    return render_template("vue_content.htm", title=title)


@blueprint.route("/", methods=["GET"])
def view_examination():
    return render_template("vue_content.htm", title=_("Examinations"))
