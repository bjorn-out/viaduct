from http import HTTPStatus
from typing import Any, Dict
from urllib.parse import parse_qs, urlencode, urljoin, urlparse, urlunparse

from flask import Blueprint, abort, redirect, render_template, request, url_for, flash
from flask_babel import _  # gettext
from flask_login import current_user
from werkzeug.routing import PathConverter, ValidationError
from werkzeug.urls import iri_to_uri

from app import app
from app.decorators import require_role
from app.exceptions.base import ResourceNotFoundException
from app.forms.page import HistoryPageForm
from app.models.page import Page, PageRevision
from app.models.redirect import Redirect
from app.roles import Roles
from app.service import (
    activity_service,
    company_service,
    navigation_service,
    news_service,
    page_service,
    redirect_service,
    role_service,
)
from app.service.committee_service import find_committee_by_page
from app.utils.htmldiff import htmldiff

blueprint = Blueprint("page", __name__)


class PagePathConverter(PathConverter):
    def to_python(self, value):
        path = super().to_python(value)
        try:
            page = page_service.get_page_by_path(path)
        # Convert RNFE to werkzeug validation error, so that the returned
        # 404 will not contain page specific details
        except ResourceNotFoundException:
            raise ValidationError()
        return page

    def to_url(self, value):
        return super().to_url(value.path)


class RedirectConverter(PathConverter):
    # pages and path converters (default weight 200) get priority over redirects
    weight = 201

    def to_python(self, value):
        path = super().to_python(value)
        try:
            redirect = redirect_service.get_by_path(path)
        # Convert RNFE to werkzeug validation error, so that the returned
        # 404 will not contain page specific details
        except ResourceNotFoundException:
            raise ValidationError()
        return redirect

    def to_url(self, value):
        return super().to_url(value.fro)


app.url_map.converters["page_path"] = PagePathConverter
app.url_map.converters["redirect_path"] = RedirectConverter


@blueprint.route("/<redirect_path:redirection>", methods=["GET"])
def get_redirect(redirection: Redirect):
    # Check if url is to our site, or an external one
    parse_result = urlparse(redirection.to)
    if parse_result.netloc == "":
        parse_result = parse_result._replace(netloc=request.host)
    if parse_result.scheme == "":
        parse_result = parse_result._replace(scheme=request.scheme)

    # get GET parameters so they can be applied to the redirected
    # URL
    if request.args:
        redirection_qs = parse_qs(parse_result.query)
        redirection_qs.update(request.args.to_dict(flat=False))
        parse_result = parse_result._replace(
            query=urlencode(redirection_qs, doseq=True)
        )

    # this is necessary to prevent incorrect escaping
    return redirect(iri_to_uri(urlunparse(parse_result)))


@blueprint.route("/<page_path:page>", methods=["GET"])
def get_page(page: Page):
    if not page_service.can_user_read_page(page, current_user):
        return abort(403)

    revision = page_service.get_latest_revision(page)
    if not revision:
        return abort(404)
    can_write = role_service.user_has_role(current_user, Roles.PAGE_WRITE)

    return _render_page(page, revision, can_write)


@blueprint.route("/<int:page_id>/history/", methods=["GET", "POST"])
@require_role(Roles.PAGE_WRITE)
def get_page_history(page_id):
    form = HistoryPageForm(request.form)

    page = page_service.get_page_by_id(page_id, incl_deleted=True)

    if not page:
        return abort(404)

    revisions = page_service.get_all_revisions(page)

    form.previous.choices = [(revision.id, "") for revision in revisions]
    form.current.choices = [(revision.id, "") for revision in revisions]

    if form.validate_on_submit():
        prev_rev_id = form.previous.data
        cur_rev_id = form.current.data

        previous_revision = page_service.get_revision_by_id(page, prev_rev_id)
        current_revision = page_service.get_revision_by_id(page, cur_rev_id)

        prev_nl, prev_en = previous_revision.get_comparable()
        cur_nl, cur_en = current_revision.get_comparable()
        diff_nl = htmldiff(prev_nl, cur_nl)
        diff_en = htmldiff(prev_en, cur_en)

        return render_template(
            "page/compare_page_history.htm",
            diffs={_("Dutch"): diff_nl, _("English"): diff_en},
        )

    return render_template(
        "page/get_page_history.htm",
        form=form,
        revisions=zip(revisions, form.previous, form.current),
        page=page,
    )


# TODO add page_path converter argument to ignore deleted.
@blueprint.route("/history/revision/<int:revision_id>/<path:path>/", methods=["GET"])
@require_role(Roles.PAGE_WRITE)
def get_page_revision(path: str, revision_id: int):
    page = page_service.get_page_by_path(path, incl_deleted=True)
    revision = page_service.get_revision_by_id(page, revision_id)
    if not revision:
        return abort(404)

    return _render_page(
        page, revision, can_write=False, title_suffix=" (" + _("History") + ")"
    )


def _render_page(
    page: Page, revision: PageRevision, can_write: bool, title_suffix: str = ""
):
    additional_context: Dict[str, Any] = {}
    if page.type == "committee":
        committee = find_committee_by_page(page)
        if not committee:
            flash(
                "This is a committee page, but no committee was linked to "
                "this page yet.",
                "danger",
            )
            return abort(404)

        additional_context["coordinator_interim"] = committee.coordinator_interim
        additional_context["open_new_members"] = committee.open_new_members
        if committee.coordinator:
            additional_context["coordinator_name"] = committee.coordinator.name
        if committee.group.maillist:
            additional_context["maillist"] = committee.group.maillist

        additional_context["committee_members"] = map(
            lambda i: i.name, committee.group.users
        )

    return render_template(
        "%s/view_single.htm" % page.type,
        page=page,
        revision=revision,
        title=revision.title + title_suffix,
        can_write=can_write,
        **additional_context
    )


@blueprint.route("/robots.txt", methods=["GET"])
def robots_txt():
    return (
        render_template("page/robots.txt"),
        HTTPStatus.OK,
        {"Content-Type": "text/plain"},
    )


@blueprint.route("/sitemap.xml", methods=["GET"])
def sitemap():
    """Generate sitemap.xml. Makes a list of urls and date modified."""
    pages = set()

    entries_all = navigation_service.get_navigation_top_entries()
    entries_all = navigation_service.remove_unauthorized(entries_all, current_user)
    for entry in entries_all:
        # Some navigation entries have urls, others are added to the sitemap
        # using the pages.
        if entry.url:
            pages.add(
                (
                    urljoin(url_for("home.home", _external=True), entry.href),
                    entry.modified,
                )
            )

    # companies
    for profile in company_service.find_all_profiles(filter_inactive=True):
        pages.add(
            (
                url_for("company.view", company=profile.company, _external=True),
                profile.modified.isoformat(),
            )
        )

    # jobs
    for job in company_service.find_all_jobs(filter_inactive=True):
        pages.add(
            (
                url_for("vue.view", job_id=job.id, _external=True),
                job.modified.isoformat(),
            )
        )

    # news
    for n in news_service.find_all():
        if n.can_read():
            pages.add(
                (url_for("news.view", news=n, _external=True), n.modified.isoformat())
            )

    # activities
    for activity in activity_service.get_activities_ending_after_now():
        pages.add(
            (
                url_for("activity.get_activity", activity=activity, _external=True),
                activity.modified.isoformat(),
            )
        )

    # standalone pages
    standalone_pages = page_service.find_all_active_pages()
    committee_pages = page_service.find_all_committee_pages()
    for page in [*standalone_pages, *committee_pages]:
        try:
            revision = page_service.get_latest_revision(page)
            pages.add(
                (
                    url_for("page.get_page", page=page, _external=True),
                    revision.modified.isoformat(),
                )
            )
        except ResourceNotFoundException:
            pass

    return (
        render_template("page/sitemap.xml", pages=pages),
        HTTPStatus.OK,
        {"Content-Type": "application/xml"},
    )
