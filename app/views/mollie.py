from http import HTTPStatus

from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from flask_babel import _
from mollie.api.objects.payment import Payment

import app.service.mollie_service
from app.decorators import require_role
from app.models.mollie import Transaction
from app.roles import Roles
from app.service import mollie_service

blueprint = Blueprint("mollie", __name__, url_prefix="/mollie")


def _get_transaction_status_description(transaction: Transaction) -> str:
    desc = {
        Payment.STATUS_PAID: _("Payment successfully received."),
        Payment.STATUS_OPEN: _("Your payment has not been completed."),
        Payment.STATUS_PENDING: _("Your payment is being processed."),
        Payment.STATUS_CANCELED: _("Your payment was cancelled"),
        "cancelled": _(
            "Your payment was cancelled"
        ),  # compat for api v2: the new version uses canceled but we use cancelled
        Payment.STATUS_FAILED: _("Your payment was failed"),
        Payment.STATUS_EXPIRED: _("Your payment was expired"),
    }

    if transaction.status in desc:
        return desc[transaction.status]
    return _("Your payment has status: %s" % transaction.status)


# Route where the user returns to after payment
@blueprint.route("/return/<callback_id>/", methods=["GET"])
def callback(callback_id: str):
    transaction = mollie_service.on_return(callback_id)

    status_description = _get_transaction_status_description(transaction)

    if transaction.callback_membership:
        if transaction.has_paid:
            return redirect(url_for("membership.complete"))
        else:
            flash(status_description, "danger")
            return redirect(url_for("membership.payment"))

    return render_template("mollie/status.htm", title=status_description)


@blueprint.route("/webhook/", methods=["POST"])
def webhook():
    """
    Webhook called by mollie, will always return 200.

    When transactions are unknown, still return 200. See:
    https://docs.mollie.com/guides/webhooks#how-to-handle-unknown-ids
    """
    mollie_id = request.form.get("id", None)
    if not mollie_id:
        return "", HTTPStatus.OK

    mollie_service.on_webhook_called(mollie_id)

    return "", HTTPStatus.OK


@blueprint.route("/list/")
@blueprint.route("/list/<string:from_id>/")
@require_role(Roles.MOLLIE_READ)
def list(from_id=None):
    payments, *args = app.service.mollie_service.get_payments(from_id)
    if payments:
        previous_payments, next_payments, _ = args
    else:
        previous_payments, next_payments = None, None
    return render_template(
        "mollie/list.htm",
        payments=payments,
        previous_payments=previous_payments,
        next_payments=next_payments,
    )


@blueprint.route("/check/", methods=["GET", "POST"])
@blueprint.route("/check/<mollie_id>/", methods=["GET", "POST"])
@require_role(Roles.FINANCIAL_ADMIN)
def check(mollie_id=None):
    transaction = mollie_service.find_transaction_for_mollie_id(mollie_id)
    if not transaction:
        abort(400)

    mollie_service.update_transaction_and_handle_callbacks(transaction)

    return redirect(url_for("mollie.list"))
