// https://vuejs.org/v2/guide/typescript.html#Augmenting-Types-for-Use-with-Plugins
/* eslint @typescript-eslint/no-unused-vars: "off"  */
// noinspection ES6UnusedImports
import Vue from "vue";

declare module "vue/types/vue" {
    interface Vue {
        $viaduct: {
            locale: string;
        };
    }
}
