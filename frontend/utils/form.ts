import { pageApi } from "../scripts/api/page";
import { WrappedFormUtils } from "ant-design-vue/types/form/form";

export function validateAntForm(form: WrappedFormUtils) {
    return new Promise((resolve, reject) => {
        form.validateFields((err, values) => {
            if (err) {
                return reject(err);
            }

            resolve(values);
        });
    });
}

export function urlValidation(rule, value, callback) {
    try {
        new URL(value);
        callback();
    } catch (_) {
        callback("Please enter a valid url");
    }
}

export function urlOptionalValidation(rule, value, callback) {
    if (value === "") callback();

    urlValidation(rule, value, callback);
}

export function pathValidation(rule, value, callback) {
    if (value && value[0] != "/" && value[value.length - 1] != "/") {
        callback();
    } else {
        callback("Please enter a path without slashes at the start or end.");
    }
}

export function emailValidation(rule, value, callback) {
    if (
        /(^$|^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$)/.test(
            value
        )
    ) {
        callback();
    } else {
        callback("Please enter a valid e-mail address.");
    }
}

export function alphaNumValidation(rule, value, callback) {
    if (/^[a-zA-Z0-9]*$/.test(value)) {
        callback();
    } else {
        callback("Please only use alpha-numeric characters.");
    }
}

export function copernicaColumnValidation(rule, value, callback) {
    if (/^[A-Za-z]\w*$/.test(value)) {
        callback();
    } else {
        callback("Please only use alpha-numeric or underscores characters.");
    }
}

export function previewRender(plainText, preview) {
    pageApi
        .preview(plainText)
        .then((value) => {
            preview.innerHTML = value.data;
        })
        .catch((e) => {
            console.error(e);
            preview.innerHTML = "An error occurred loading the preview...";
        });
    return "Loading...";
}
