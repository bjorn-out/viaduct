interface FlaskJsGlue {
    url_for: (route: string, params?: Record<string, unknown>) => string;
}
declare global {
    interface Window {
        Flask: FlaskJsGlue;
        viaduct: { locale: string };
    }
}
declare let Flask: FlaskJsGlue;

export default Flask;
