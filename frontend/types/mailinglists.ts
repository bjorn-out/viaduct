export class MailingList {
    public id: number;
    public nl_name = "";
    public en_name = "";
    public copernica_column_name = "";
    public members_only = false;
    public default = false;

    public subscribed = false;
}
