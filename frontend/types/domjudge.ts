export class DOMjudgeContestSettings {
    public banner_url: string | null;
}

export class DOMjudgeContest extends DOMjudgeContestSettings {
    public id: number;
    public name: string;
    public banner_file_id: number;
}

export class DOMjudgeContestUser {
    public id: number;
    public team_id: number;
    public name: string;
    public email: string;
}
