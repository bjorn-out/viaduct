export class Group {
    public id: number;
    public name: string;
    public maillist: string;
    public mailtype: string;
}
