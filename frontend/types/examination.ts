export class Course {
    public id: number;
    public name: string;
    public datanose_code: string;
}

export class Education {
    public id: number;
    public nl_name: string;
    public en_name: string;
}
