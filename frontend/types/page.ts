export class MultilangString {
    constructor(en: string, nl: string) {
        this.en = en;
        this.nl = nl;
    }

    public en: string;
    public nl: string;
}

export class PageBody {
    public path = "";
    public type?: string = "";
    public require_membership_to_view = false;
}

export class Page extends PageBody {
    public id = 0;
    public deleted: string | null = "";
}

export class PageRevision {
    public title = new MultilangString("", "");
    public content = new MultilangString("", "");
    public revision_comment = "";
}
