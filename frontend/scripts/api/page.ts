import { Api, PaginatedResponse } from "./api";
import { AxiosResponse } from "axios";
import Flask from "../../utils/flask";
import { Page, PageBody, PageRevision } from "../../types/page";

export class PageRenderResponse {
    page_id: number;
    title: string;
    content: string;
}

class PageApi extends Api {
    public async getPage(pageId: number): Promise<AxiosResponse<Page>> {
        return this.get<Page>(Flask.url_for("api.page", { page: pageId }));
    }

    public async getLatestRevision(
        pageId: number
    ): Promise<AxiosResponse<PageRevision>> {
        return this.get<PageRevision>(
            Flask.url_for("api.page_revision.latest", { page_id: pageId })
        );
    }

    public async preview(content: string): Promise<AxiosResponse<string>> {
        return this.post(Flask.url_for("api.page_preview", {}), {
            content: content,
        });
    }

    async renderPage(
        path: string,
        locale: string
    ): Promise<AxiosResponse<PageRenderResponse>> {
        return this.get<PageRenderResponse>(
            Flask.url_for("api.page.preview", {
                lang: locale,
                path: path,
            })
        );
    }

    async createPage(pageBody: PageBody): Promise<AxiosResponse<Page>> {
        return this.post<Page>(Flask.url_for("api.pages"), pageBody);
    }

    async updatePage(pageId: number, pageBody: PageBody) {
        return this.put<Page>(
            Flask.url_for("api.page", { page: pageId }),
            pageBody
        );
    }

    async newRevision(pageId: number, revisionBody: PageRevision) {
        return this.post<PageRevision>(
            Flask.url_for("api.page_revision.new", { page_id: pageId }),
            revisionBody
        );
    }

    async findPages(search: string, page: number) {
        return this.get<PaginatedResponse<Page>>(
            Flask.url_for("api.pages", {
                search: search,
                page: page,
            })
        );
    }
}

export const pageApi = new PageApi();
