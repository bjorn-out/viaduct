import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";

export class CourseBody {
    name?: string;
    datanose_code?: string;
}

export class Course extends CourseBody {
    id: number;
}

type CourseExaminations = {
    id: number;
    comment: string;
    date: string;
    examination_file_id: number | null;
    answers_file_id: number | null;
    course: Course;
    test_type: string;
};
type ExaminationType = "Mid-term" | "End-term" | "Retake" | "Unknown";
type ExaminationResponse = {
    id: number;
    course: Course;

    date: string;

    comment: string;
    answers_file_id: number;
    examination_file_id: number;

    test_type: ExaminationType;
};
type ExaminationRequest = {
    course_id: number;
    date: string;
    comment: string;
    test_type: ExaminationType;
};

class ExaminationApi extends Api {
    createCourse(values: CourseBody): AxiosPromise<Course> {
        return this.post<Course>(Flask.url_for("api.courses"), values);
    }

    updateCourse(courseId: number, values: CourseBody): AxiosPromise<Course> {
        return this.put<Course>(
            Flask.url_for("api.course", { course: courseId }),
            values
        );
    }

    deleteCourse(courseId: string): AxiosPromise<void> {
        return this.delete<void>(
            Flask.url_for("api.course", { course: courseId })
        );
    }

    getExamsByCourse(courseId: number) {
        return this.get<CourseExaminations[]>(
            Flask.url_for("api.course.examinations", { course: courseId })
        );
    }

    async deleteExamination(examinationId: number) {
        return this.delete(
            Flask.url_for("api.examination", { examination: examinationId })
        );
    }

    async getExam(id: number) {
        return this.get<ExaminationResponse>(
            Flask.url_for("api.examination", { examination: id })
        );
    }

    async createExam(body: ExaminationRequest) {
        return this.post<ExaminationResponse>(
            Flask.url_for("api.examinations"),
            body
        );
    }

    async uploadExamFile(examId: number, file: File, type: "answers" | "exam") {
        const formData = new FormData();
        formData.append("file", file);
        return this.post<void>(
            Flask.url_for(`api.examination.${type}`, { examination: examId }),
            formData
        );
    }

    async updateExam(examId: number, values: ExaminationRequest) {
        return this.put<Course>(
            Flask.url_for("api.examination", { examination: examId }),
            values
        );
    }
}

export const examinationApi = new ExaminationApi();
