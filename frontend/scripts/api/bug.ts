import { Api } from "./api";
import Flask from "../../utils/flask";

export interface BugRequest {
    project: "pos" | "pretix" | "viaduct";
    title: string;
    description: string;
}

export interface BugResponse {
    id: number;
    title: string;
    web_url: string;
}

class BugApi extends Api {
    createBug(r: BugRequest) {
        return this.post<BugResponse>(Flask.url_for("api.bugs.report"), r);
    }
}

export const bugApi = new BugApi();
