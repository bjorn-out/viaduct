import { Api, PaginatedResponse } from "./api";
import { AxiosPromise } from "axios";
import { MultilangString } from "../../types/page";
import Flask from "../../utils/flask";

export type ActivityBody = {
    name: MultilangString;
    description: MultilangString;
    start_time: string;
    end_time: string;
    location: string;
    pretix_event_slug: string;
};

export type Activity = {
    id: number;
    created: string;
} & ActivityBody;

class ActivityApi extends Api {
    createActivity(body: ActivityBody): AxiosPromise<Activity> {
        return this.post<Activity>(Flask.url_for("api.activities", {}), body);
    }

    updateActivity(
        activityId: number,
        body: ActivityBody
    ): AxiosPromise<Activity> {
        return this.put<Activity>(
            Flask.url_for("api.activity", { activity: activityId }),
            body
        );
    }

    setActivityPicture(activityId, blob: Blob): AxiosPromise<void> {
        const formData = new FormData();
        formData.append("file", blob);
        return this.put<void>(
            Flask.url_for("api.activities.picture", { activity: activityId }),
            formData
        );
    }

    searchActivities(
        page: number,
        search: string
    ): AxiosPromise<PaginatedResponse<Activity>> {
        return this.get<PaginatedResponse<Activity>>(
            Flask.url_for("api.activities", {
                page: page,
                limit: 15,
                search: search,
            })
        );
    }
}

export const activityApi = new ActivityApi();
