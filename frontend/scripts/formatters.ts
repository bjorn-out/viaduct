export function date(value) {
    const locale = window.navigator.language || "nl-NL";
    const config = {
        year: "numeric",
        month: "long",
        day: "numeric",
    };
    return new Date(value).toLocaleDateString(locale, config);
}

export function datetime(value) {
    const locale = window.navigator.language || "nl-NL";
    const config = {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    };
    return new Date(value).toLocaleString(locale, config);
}
