import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import TutoringRequest from "./tutoring_request.vue";
import { i18n } from "../../translations";

jest.mock("../../utils/flask.ts");
jest.mock("../../scripts/api/course.ts");
const localVue = createLocalVue();
localVue.use(VueI18n);

const consoleSpy = jest.spyOn(console, "error");

describe(TutoringRequest.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutoringRequest, {
            localVue: localVue,
            stubs: ["router-link"],
            i18n: i18n,
            mocks: {
                $route: {
                    params: {},
                    query: {
                        courseId: 1,
                    },
                },
            },
        });

        getByText("Tutoring request");
    });
});
