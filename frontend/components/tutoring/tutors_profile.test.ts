import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import TutorsProfile from "./tutors_profile.vue";
import { i18n } from "../../translations";
import VueRouter from "vue-router";
import flushPromises from "flush-promises";

jest.mock("../../utils/flask.ts");
jest.mock("../../scripts/api/page.ts");
jest.mock("../../scripts/api/tutoring.ts");
jest.mock("../../scripts/api/course.ts");
jest.mock("../../scripts/api/user.ts");
const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(VueRouter);

const consoleSpy = jest.spyOn(console, "error");

describe(TutorsProfile.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutorsProfile, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
            mocks: { $viaduct: { locale: "en" } },
        });
        await flushPromises();
        getByText("tutors/self en content");
        getByText("Advanced Networking tutor course");
    });
});
