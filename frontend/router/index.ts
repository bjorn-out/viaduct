import VueRouter, { Route } from "vue-router";
import { authStore } from "../store/auth";
import { EsModuleComponent } from "vue/types/options";

const RedirectOverview = (): Promise<EsModuleComponent> =>
    import("../components/redirect/redirect_overview.vue");

const PimpyCreateMinute = (): Promise<EsModuleComponent> =>
    import("../components/pimpy/create_minute.vue");
const PimpyCreateTask = (): Promise<EsModuleComponent> =>
    import("../components/pimpy/create_task.vue");
const PimpyBase = (): Promise<EsModuleComponent> =>
    import("../components/pimpy/pimpy_base.vue");
const PimpyTasks = (): Promise<EsModuleComponent> =>
    import("../components/pimpy/pimpy_tasks.vue");
const PimpyMinutes = (): Promise<EsModuleComponent> =>
    import("../components/pimpy/pimpy_minutes.vue");
const PimpySingleMinute = (): Promise<EsModuleComponent> =>
    import("../components/pimpy/pimpy_single_minute.vue");
const UserOverview = (): Promise<EsModuleComponent> =>
    import("../components/user/user_overview.vue");
const MembersOfMerit = (): Promise<EsModuleComponent> =>
    import("../components/user/MembersOfMerit.vue");
const SearchIndex = (): Promise<EsModuleComponent> =>
    import("../components/search/search_index.vue");
const TwoFactorAuthentication = (): Promise<EsModuleComponent> =>
    import("../components/user/two_factor_authentication.vue");
const EducationOverview = (): Promise<EsModuleComponent> =>
    import("../components/examination/education_overview.vue");
const ChallengeOverview = (): Promise<EsModuleComponent> =>
    import("../components/challenge/challenge_overview.vue");
const EditChallenge = (): Promise<EsModuleComponent> =>
    import("../components/challenge/edit_challenge.vue");
const ChallengeAdminSubmission = (): Promise<EsModuleComponent> =>
    import("../components/challenge/ChallengeAdminSubmission.vue");
const DeclarationOverview = (): Promise<EsModuleComponent> =>
    import("../components/declaration/declaration_overview.vue");

const NavigationOverview = (): Promise<EsModuleComponent> =>
    import("../components/navigation/navigation_overview.vue");
const EditNavigation = (): Promise<EsModuleComponent> =>
    import("../components/navigation/edit_navigation.vue");
const EditMeeting = (): Promise<EsModuleComponent> =>
    import("../components/meeting/edit_meeting.vue");
const EditALV = (): Promise<EsModuleComponent> =>
    import("../components/alv/edit_alv.vue");
const AlvOverview = (): Promise<EsModuleComponent> =>
    import("../components/alv/alv_overview.vue");
const AlvDetailsPage = (): Promise<EsModuleComponent> =>
    import("../components/alv/alv_details_page.vue");

const MailingListsSubscriptionSelectorProfile = (): Promise<
    EsModuleComponent
> => import("../components/mailinglists/subscription_selector_profile.vue");

const GroupUserOverview = (): Promise<EsModuleComponent> =>
    import("../components/group/group_user_overview.vue");
const GroupOverview = (): Promise<EsModuleComponent> =>
    import("../components/group/group_overview.vue");
const GroupRoles = (): Promise<EsModuleComponent> =>
    import("../components/group/group_roles.vue");
const EditGroup = (): Promise<EsModuleComponent> =>
    import("../components/group/edit_group.vue");

const EditActivity = (): Promise<EsModuleComponent> =>
    import("../components/activity/edit_activity.vue");
const EditNewsletter = (): Promise<EsModuleComponent> =>
    import("../components/newsletter/edit_newsletter.vue");
const EditNews = (): Promise<EsModuleComponent> =>
    import("../components/news/edit_news.vue");
const NewsletterOverview = (): Promise<EsModuleComponent> =>
    import("../components/newsletter/NewsletterOverview.vue");

const UserEditAdminDataForm = (): Promise<EsModuleComponent> =>
    import("../components/user/edit_admin_data.vue");
const UserOAuthApplications = (): Promise<EsModuleComponent> =>
    import("../components/user/oauth_applications.vue");

const TutoringOverview = (): Promise<EsModuleComponent> =>
    import("../components/tutoring/tutoring_overview.vue");
const TutoringRequest = (): Promise<EsModuleComponent> =>
    import("../components/tutoring/tutoring_request.vue");
const TutorOverview = (): Promise<EsModuleComponent> =>
    import("../components/tutoring/tutors_overview.vue");
const TutorCourseRegister = (): Promise<EsModuleComponent> =>
    import("../components/tutoring/tutor_course_register.vue");
const TutorsProfile = (): Promise<EsModuleComponent> =>
    import("../components/tutoring/tutors_profile.vue");

const CourseOverview = (): Promise<EsModuleComponent> =>
    import("../components/examination/course_overview.vue");
const EditCourse = (): Promise<EsModuleComponent> =>
    import("../components/examination/edit_course.vue");
const CreateCourse = (): Promise<EsModuleComponent> =>
    import("../components/examination/create_course.vue");
const EditExamination = (): Promise<EsModuleComponent> =>
    import("../components/examination/edit_examination.vue");
const ExaminationOverview = (): Promise<EsModuleComponent> =>
    import("../components/examination/examination_overview.vue");
const AdminOverview = (): Promise<EsModuleComponent> =>
    import("../components/company/admin_overview.vue");
const EditCompany = (): Promise<EsModuleComponent> =>
    import("../components/company/edit_company.vue");
const EditCompanyJobForm = (): Promise<EsModuleComponent> =>
    import("../components/company/edit_job.vue");
const EditCompanyBannerForm = (): Promise<EsModuleComponent> =>
    import("../components/company/edit_banner.vue");
const EditCompanyProfileForm = (): Promise<EsModuleComponent> =>
    import("../components/company/edit_profile.vue");
const CompanyJobOverview = (): Promise<EsModuleComponent> =>
    import("../components/company/company_job_overview.vue");
const CompanyJob = (): Promise<EsModuleComponent> =>
    import("../components/company/company_job.vue");
const CompanyOverview = (): Promise<EsModuleComponent> =>
    import("../components/company/company_overview.vue");
const CompanyProfile = () =>
    import("../components/company/company_profile.vue");

const AlbumList = (): Promise<EsModuleComponent> =>
    import("../components/photos/AlbumList.vue");
const PhotoList = (): Promise<EsModuleComponent> =>
    import("../components/photos/PhotoList.vue");
const PhotoModule = (): Promise<EsModuleComponent> =>
    import("../components/photos/PhotoModule.vue");

const PageOverview = (): Promise<EsModuleComponent> =>
    import("../components/page/page_overview.vue");
const EditPage = (): Promise<EsModuleComponent> =>
    import("../components/page/edit_page.vue");

const CommitteeOverview = (): Promise<EsModuleComponent> =>
    import("../components/committee/committee_overview.vue");

const EditCommittee = (): Promise<EsModuleComponent> =>
    import("../components/committee/edit_committee.vue");

const MailingListsOverview = (): Promise<EsModuleComponent> =>
    import("../components/mailinglists/overview.vue");
const EditMailingListForm = (): Promise<EsModuleComponent> =>
    import("../components/mailinglists/edit_mailinglist.vue");

const AdminAsync = (): Promise<EsModuleComponent> =>
    import("../components/admin/Async.vue");

const ContestEditForm = (): Promise<EsModuleComponent> =>
    import("../components/domjudge/ContestEditForm.vue");
const ContestTeamsOverview = (): Promise<EsModuleComponent> =>
    import("../components/domjudge/ContestUsersOverview.vue");

const BugReport = (): Promise<EsModuleComponent> =>
    import("../components/bug/BugReport.vue");

const PageNotFound = (): Promise<EsModuleComponent> =>
    import("../components/errors/404.vue");

export const routes = [
    { path: "/admin/async/", name: "admin-async", component: AdminAsync },
    { path: "/bug/report/", name: "bug-report", component: BugReport },
    { path: "/courses/", name: "courses", component: CourseOverview },
    {
        path: "/courses/create/",
        name: "course-create",
        component: CreateCourse,
    },
    {
        path: "/courses/:courseId/edit/",
        name: "course-edit",
        component: EditCourse,
    },
    { path: "/education/", name: "educations", component: EducationOverview },
    {
        path: "/tutoring/",
        name: "tutoring-overview",
        component: TutoringOverview,
    },
    {
        path: "/tutoring/request/",
        name: "tutoring-request",
        component: TutoringRequest,
    },
    { path: "/tutors/", name: "tutor-overview", component: TutorOverview },
    {
        path: "/tutors/:userId/",
        name: "tutor-profile",
        component: TutorsProfile,
    },
    {
        path: "/tutors/:userId/course/",
        name: "tutor-course-register",
        component: TutorCourseRegister,
    },
    {
        path: "/tutoring/:tutoringId/",
        name: "tutoring-details",
        component: TutoringRequest,
    },
    {
        path: "/pages/",
        name: "pages",
        component: PageOverview,
        meta: { title: "Pages" },
    },
    {
        path: "/pages/create/",
        name: "create-page",
        component: EditPage,
        meta: { title: "Create page" },
    },
    {
        path: "/pages/:pageId/edit/",
        name: "edit-page",
        component: EditPage,
        meta: { title: "Edit page" },
    },
    {
        path: "/commissie/",
        name: "committee-overview",
        component: CommitteeOverview,
    },
    {
        path: "/committee/create/",
        name: "committee-create",
        component: EditCommittee,
        meta: { title: "Create committee" },
    },
    {
        path: "/committee/:committeeId/edit/",
        name: "committee-edit",
        component: EditCommittee,
        meta: { title: "Edit committee" },
    },
    {
        path: "/examination/",
        name: "examination-overview",
        component: ExaminationOverview,
    },
    {
        path: "/examination/create/",
        name: "examination-create",
        component: EditExamination,
    },
    {
        path: "/examination/:examinationId/edit/",
        name: "examination-edit",
        component: EditExamination,
    },
    { path: "/users/", name: "users", component: UserOverview },
    {
        path: "/members-of-merit/",
        name: "members-of-merit",
        alias: ["/ereleden/"],
        component: MembersOfMerit,
    },
    { path: "/search/", name: "search", component: SearchIndex },
    {
        path: "/groups/",
        name: "group-overview",
        meta: { title: "Groups" },
        component: GroupOverview,
    },
    {
        path: "/groups/create/",
        name: "group-create",
        meta: { title: "Create group" },
        component: EditGroup,
    },
    {
        path: "/groups/:groupId/edit/",
        name: "group-edit",
        meta: { title: "Edit group" },
        component: EditGroup,
    },
    {
        path: "/groups/:groupId/users/",
        name: "group-users",
        component: GroupUserOverview,
    },
    {
        path: "/groups/:groupId/roles/",
        name: "group-roles",
        component: GroupRoles,
    },
    {
        path: "/photos/",
        component: PhotoModule,
        children: [
            {
                path: "/photos/",
                name: "Albums",
                component: AlbumList,
            },
            {
                path: "/photos/:id/",
                name: "Album",
                component: PhotoList,
                meta: { scrollIgnore: true },
            },
            {
                path: "/photos/:id/:photoid/",
                name: "Photo",
                component: PhotoList,
                meta: { scrollIgnore: true },
            },
        ],
    },
    {
        path: "/jobs/",
        name: "company-jobs",
        component: CompanyJobOverview,
    },
    {
        path: "/jobs/:jobId/",
        name: "view-company-job",
        component: CompanyJob,
        props: true,
    },
    {
        path: "/companies/",
        name: "company",
        component: CompanyOverview,
    },
    {
        path: "/companies/:companySlug/",
        name: "company-profile",
        component: CompanyProfile,
        props: true,
    },
    {
        path: "/admin/company/",
        name: "company-admin",
        component: AdminOverview,
    },
    {
        path: "/admin/company/:companyId?",
        name: "edit-company",
        component: EditCompany,
    },
    {
        path: "/admin/company/:companyId/jobs/:jobId?",
        name: "edit-company-job",
        component: EditCompanyJobForm,
    },
    {
        path: "/admin/company/:companyId/banner/",
        name: "edit-company-banner",
        component: EditCompanyBannerForm,
    },
    {
        path: "/admin/company/:companyId/profile/",
        name: "edit-company-profile",
        component: EditCompanyProfileForm,
    },
    {
        path: "/users/self/tfa/",
        name: "two-factor-authentication",
        component: TwoFactorAuthentication,
    },
    {
        path: "/users/:userId/mailinglist-subscriptions/",
        name: "mailinglists-subscriptions",
        component: MailingListsSubscriptionSelectorProfile,
    },
    {
        path: "/users/:userId/admin/",
        name: "user-admin",
        component: UserEditAdminDataForm,
    },
    {
        path: "/users/:userId/applications/",
        name: "user-applications",
        component: UserOAuthApplications,
    },
    {
        path: "/mailinglists/",
        name: "mailinglists-overview",
        meta: { title: "Mailing lists overview" },
        component: MailingListsOverview,
    },
    {
        path: "/mailinglists/:mailingListId/edit/",
        name: "mailinglist-edit",
        meta: { title: "Edit mailing list" },
        component: EditMailingListForm,
    },
    {
        path: "/mailinglists/create/",
        name: "mailinglist-create",
        meta: { title: "Create mailing list" },
        component: EditMailingListForm,
    },
    {
        path: "/navigation/",
        name: "navigation-overview",
        meta: { title: "Navigation" },
        component: NavigationOverview,
    },
    {
        path: "/navigation/create/",
        name: "navigation-create",
        meta: { title: "Create navigation entry" },
        component: EditNavigation,
    },
    {
        path: "/navigation/:entryId/edit/",
        name: "navigation-edit",
        meta: { title: "Edit navigation entry" },
        component: EditNavigation,
    },
    {
        path: "/activities/create/",
        name: "activities-create",
        meta: { title: "Create activity" },
        component: EditActivity,
    },
    {
        path: "/activities/:activityId/edit/",
        name: "activities-edit",
        meta: { title: "Edit activity" },
        component: EditActivity,
    },
    {
        path: "/challenge/",
        name: "challenge-overview",
        meta: { title: "Challenges" },
        component: ChallengeOverview,
    },
    {
        path: "/challenge/:challengeId/edit/",
        name: "challenge-edit",
        meta: { title: "Edit challenge" },
        component: EditChallenge,
    },
    {
        path: "/challenge/:challengeId/add-manual-submission/",
        name: "challenge-admin-submission",
        meta: { title: "Add manual submission" },
        component: ChallengeAdminSubmission,
    },
    {
        path: "/challenge/create/",
        name: "challenge-create",
        meta: { title: "Create challenge" },
        component: EditChallenge,
    },
    {
        path: "/newsletter/",
        name: "newsletter-overview",
        component: NewsletterOverview,
    },
    {
        path: "/newsletter/:newsletterId/edit/",
        name: "newsletter-edit",
        component: EditNewsletter,
    },
    {
        path: "/newsletter/create/",
        name: "newsletter-create",
        component: EditNewsletter,
    },
    {
        path: "/news/:newsId/edit/",
        name: "news-edit",
        component: EditNews,
    },
    {
        path: "/news/create/",
        name: "news-create",
        component: EditNews,
    },
    {
        path: "/declaration/",
        name: "declaration",
        component: DeclarationOverview,
    },
    {
        path: "/meeting/:meetingId/edit/",
        name: "meeting-edit",
        component: EditMeeting,
    },
    {
        path: "/meeting/create/",
        name: "meeting-create",
        component: EditMeeting,
    },
    {
        path: "/pimpy/",
        component: PimpyBase,
        // All routes use the prop function to decouple the component from
        // the $route variable in vue, making testing easier.
        props: (route: Route): unknown => ({
            self: !!route.meta.self,
            type: route.meta.type,
            groupId: parseInt(route.query.group as string) || null,
        }),
        children: [
            {
                path: "/pimpy/",
                name: "pimpy-base",
                component: PimpyTasks,
                props: {
                    self: true,
                    groupId: null,
                },
                meta: { type: "tasks", self: true },
            },
            {
                path: "tasks/create/",
                name: "pimpy-task-create",
                component: PimpyCreateTask,
                props: (route: Route): unknown => ({
                    self: false,
                    groupId: parseInt(route.query.group as string) || null,
                }),
                meta: { type: "tasks", self: false },
            },
            {
                path: "tasks/",
                name: "pimpy-tasks",
                component: PimpyTasks,
                props: (route: Route): unknown => ({
                    groupId: parseInt(route.query.group as string) || null,
                }),
                meta: { type: "tasks", self: false },
            },
            {
                path: "tasks/self/",
                name: "pimpy-tasks-self",
                component: PimpyTasks,
                props: (route: Route): unknown => ({
                    self: true,
                    groupId: parseInt(route.query.group as string) || null,
                }),
                meta: { type: "tasks", self: true },
            },
            {
                path: "minutes/create/",
                name: "pimpy-minute-create",
                component: PimpyCreateMinute,
                props: (route: Route): unknown => ({
                    groupId: parseInt(route.query.group as string) || null,
                }),
                meta: { type: "minutes" },
            },
            {
                path: "minutes/single/:minuteId/:line?/",
                name: "pimpy-minute-single",
                component: PimpySingleMinute,
                meta: { type: "minutes" },
            },
            {
                path: "minutes/",
                name: "pimpy-minutes",
                component: PimpyMinutes,
                props: (route: Route): unknown => ({
                    groupId: parseInt(route.query.group as string) || null,
                }),
                meta: { type: "minutes" },
            },
        ],
    },
    {
        path: "/alv/:alvId/edit/:tab?",
        name: "alv-edit",
        component: EditALV,
    },
    {
        path: "/alv/create/",
        name: "alv-create",
        component: EditALV,
    },
    {
        path: "/alv/:alvId/",
        name: "alv-details",
        component: AlvDetailsPage,
    },
    {
        path: "/alv/",
        name: "alv-overview",
        component: AlvOverview,
    },
    {
        path: "/redirect/",
        name: "redirect",
        component: RedirectOverview,
    },
    {
        path: "/domjudge/contest/:contestId/edit/",
        name: "domjudge-contest-edit",
        component: ContestEditForm,
    },
    {
        path: "/domjudge/contest/:contestId/users/",
        name: "domjudge-contest-users",
        component: ContestTeamsOverview,
    },
    {
        path: "/membership/complete/",
        name: "membership-communication",
        component: MailingListsSubscriptionSelectorProfile,
    },
    {
        path: "*",
        component: PageNotFound,
    },
];

export const router = new VueRouter({
    mode: "history",
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (to.meta.scrollIgnore) {
            return;
        }

        // Do not scroll when the query parameters change.
        if (from.name === to.name && from.path === to.path) {
            return;
        }
        if (savedPosition) {
            return savedPosition;
        }
        return { x: 0, y: 0 };
    },
});

router.beforeEach(async (to, from, next) => {
    await authStore.reloadRoles();
    next();
});

router.afterEach((to, from) => {
    if (to.meta.title) document.title = to.meta.title + " - via";

    to.meta.from = from;
});
