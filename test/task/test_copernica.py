import json
import pytest

from celery.signals import task_sent
from app.task.copernica import synchronize_copernica_disabling_selection_profiles
from app.models.mailinglist_model import MailingList
from app.models.setting_model import Setting


@pytest.fixture
def copernica_subscribed_user(db_session, user_factory):
    mailinglist = MailingList(
        nl_name="Copernica nieuwsbrief",
        en_name="Copernica Newsletter",
        copernica_column_name="Ingeschreven",
        members_only=True,
    )

    db_session.add(mailinglist)

    user = user_factory()
    user.copernica_id = 1337
    user.mailinglists = [mailinglist]

    db_session.add(user)
    db_session.commit()
    return user


def test_disable_profiles(app, db_session, copernica_subscribed_user, requests_mocker):
    app.config["COPERNICA_ENABLED"] = True
    db_session.add(
        Setting(key="COPERNICA_DISABLING_SELECTIONS", value=json.dumps({"Bounces": 1}))
    )
    db_session.commit()

    @task_sent.connect
    def assert_update_tasks(*args, **kwargs):
        assert kwargs["sender"] == "app.task.copernica.update_user"
        assert kwargs["args"] == (copernica_subscribed_user.id,)

    requests_mocker.get("https://api.copernica.com/v2/view/1/profileids", json=[1337])

    # Call the task-function directly, skipping worker communication.
    synchronize_copernica_disabling_selection_profiles()

    assert not copernica_subscribed_user.mailinglists.count()
