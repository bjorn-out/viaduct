import json
from datetime import datetime, timedelta, timezone
from unittest.mock import patch

from app.models.group import UserGroupHistory
from app.utils import google


def test_group_members(admin_client, admin_group, admin_user):
    rv = admin_client.get(f"/api/groups/{admin_group.id}/users")
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert admin_user.email in data["data"][0]["email"]


def test_group_members_history(db, admin_client, admin_group, admin_user):
    rv = admin_client.delete(
        f"/api/groups/{admin_group.id}/users", json={"user_ids": [admin_user.id]}
    )
    assert rv.status_code == 204
    # admin_user was the only user of admin_group, it should now be empty.
    assert not admin_group.users.all()
    history = (
        db.session.query(UserGroupHistory)
        .filter(UserGroupHistory.group_id == admin_group.id)
        .first()
    )
    assert history.user_id == admin_user.id
    assert history.deleted - datetime.now(timezone.utc) < timedelta(minutes=1)


def test_create_group_without_maillist(admin_client):
    rv = admin_client.post("/api/groups/", json=dict(mailtype="none", name="name"))
    assert rv.status_code == 201


@patch.object(google, "create_group_if_not_exists")
def test_create_group_with_maillist(mock_method, admin_client):
    rv = admin_client.post(
        "/api/groups/",
        json=dict(mailtype="mailinglist", name="name", maillist="maillist"),
    )
    assert rv.status_code == 201
    mock_method.assert_called_once()
