import pydantic
import pytest

from app.api.schema import schema_registry


@pytest.mark.parametrize("schema", ["BugReportRequest", "ChallengeAdminSubmission"])
def test_schema_api(anonymous_client, schema):
    rv = anonymous_client.get(f"/api/_schema/{schema}")
    assert rv.status_code == 200, rv.json


def test_duplicate_register():
    with pytest.raises(ValueError):

        @schema_registry.register
        @schema_registry.register
        class _A(pydantic.BaseModel):
            pass
