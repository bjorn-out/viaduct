from datetime import datetime, timedelta, timezone


def test_create_without_endtime(admin_client, admin_group):
    start_time = datetime.now(tz=timezone.utc)
    rv = admin_client.post(
        "/api/meetings/",
        json={
            "name": "Meeting",
            "description": "",
            "group_id": admin_group.id,
            "start_time": start_time.isoformat(),
            "location": "VIA",
            "cancelled": False,
        },
    )
    assert rv.status_code == 201, rv.json

    rv = admin_client.get(f"/api/meetings/{rv.json['id']}")
    assert rv.status_code == 200, rv.json
    assert datetime.fromisoformat(rv.json["end_time"]) - start_time == timedelta(
        hours=1
    )
