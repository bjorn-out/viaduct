import pytest

from conftest import CustomClient

redirect_tests = [
    ("a", "someurl", {}, "http://localhost/someurl"),
    ("c", "https://a.nl/xd", {}, "https://a.nl/xd"),
    ("d", "https://a.nl/xd?test=test", {}, "https://a.nl/xd?test=test"),
    ("e", "https://a.nl/xd?query1=aa", {"query1": "a"}, "https://a.nl/xd?query1=a"),
    (
        "e",
        "https://a.nl/xd?query1=aa",
        {"query2": "b"},
        "https://a.nl/xd?query1=aa&query2=b",
    ),
]


@pytest.mark.parametrize("fro,to,qs,to_expected", redirect_tests)
def test_redirect(
    fro: str,
    to: str,
    qs: dict,
    to_expected: str,
    db_session,
    admin_client: CustomClient,
    redirect_factory,
):
    redirect = redirect_factory()
    redirect.fro = fro
    redirect.to = to
    db_session.commit()

    rv = admin_client.get(fro, follow_redirects=False, query_string=qs)

    assert rv.status_code == 302
    assert rv.location == to_expected


def test_create_redirect(admin_client):
    rv = admin_client.get("/api/redirect/")
    assert rv.status_code == 200

    rv = admin_client.put(
        "/api/redirect/",
        json={"fro": "shorturl", "to": "/somelongerurl"},
    )
    assert rv.status_code == 200

    rv = admin_client.get("/shorturl", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith("/somelongerurl")


def test_create_redirect_case(admin_client):
    fro = "shortURL"
    to = "/somelongerurl"

    rv = admin_client.put(
        "/api/redirect/",
        json={"fro": fro, "to": to},
    )
    assert rv.status_code == 200

    rv = admin_client.get(f"/{fro}", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith(to)

    rv = admin_client.get(f"/{fro.upper()}", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith(to)

    rv = admin_client.get(f"/{fro.lower()}", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith(to)


def test_create_duplicate_redirect(admin_client):
    rv = admin_client.put(
        "/api/redirect/",
        json={"fro": "shorturl", "to": "/somelongerurl"},
    )
    assert rv.status_code == 200

    rv = admin_client.put(
        "/api/redirect/",
        json={"fro": "shorturl", "to": "/secondlongurl"},
        follow_redirects=False,
    )
    # because we're upserting now, we don't expect an error
    assert rv.status_code == 200


def test_delete_redirect(admin_client, redirect_factory):
    redirect = redirect_factory()
    rv = admin_client.get(f"/{redirect.fro}", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith(redirect.to)

    rv = admin_client.delete(f"/api/redirect/{redirect.id}/")
    assert rv.status_code == 204

    rv = admin_client.get(f"/{redirect.fro}")
    assert rv.status_code == 404
