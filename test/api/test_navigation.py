import pytest


def clean_for_put(data):
    return [{"id": d["id"], "children": clean_for_put(d["children"])} for d in data]


def test_ordering(admin_client, sample_navigation):
    rv = admin_client.get("/api/navigation/ordering")
    assert rv.status_code == 200

    for entry in rv.json:
        assert "id" in entry
        assert "children" in entry
        if entry["nl_title"] == "URL internal":
            assert len(entry["children"])
    rv = admin_client.put("/api/navigation/ordering", json=clean_for_put(rv.json))
    assert rv.status_code == 204, rv.json


@pytest.mark.parametrize(
    "data",
    [
        {"type": "url", "url": "users/", "external": False},
        {"type": "page", "page_id": None},  # filled in test with fixture
        {"type": "activities"},
    ],
)
def test_create_update_entry(data, page_revision, admin_client):
    if data["type"] == "page":
        page, page_revision = page_revision
        data["page_id"] = page.id

    data["nl_title"] = "nl_title"
    data["en_title"] = "en_title"
    data["order_children_alphabetically"] = False
    rv = admin_client.post("/api/navigation", json=data)
    assert rv.status_code == 200, rv.json

    rv = admin_client.put(f"/api/navigation/{rv.json['id']}", json=data)
    assert rv.status_code == 200, rv.json


def test_delete_entry(admin_client, sample_navigation):
    rv = admin_client.delete(f"/api/navigation/{sample_navigation[0].id}")
    assert rv.status_code == 204

    news_entry = sample_navigation[2]
    assert not news_entry.page_id
    rv = admin_client.delete(
        f"/api/navigation/{news_entry.id}",
        query_string={"include_page": True},
    )
    assert rv.status_code == 409
    assert "children" in rv.json["detail"]

    rv = admin_client.get("/")
    assert rv.status_code == 200
    assert sample_navigation[0].en_title not in rv.data.decode()

    page_entry = sample_navigation[1]
    assert page_entry.page_id
    rv = admin_client.delete(
        f"/api/navigation/{page_entry.id}",
        query_string={"include_page": True},
    )
    assert rv.status_code == 204

    pos_entry = sample_navigation[-1]
    assert pos_entry.external
    assert pos_entry.url == "pos.svia.nl"
    rv = admin_client.delete(
        f"/api/navigation/{pos_entry.id}",
        query_string={"include_page": True},
    )
    assert rv.status_code == 409
    assert "page" in rv.json["detail"]
