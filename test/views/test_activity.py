import pytest

from app import hashfs
from app.models.activity import Activity
from conftest import CustomClient


def test_create_activity(admin_client, admin_user):
    admin_client.login(admin_user)

    rv = admin_client.get("/activities/create/")
    assert rv.status_code == 200


def test_edit_activity(admin_client, admin_user, activity_factory):
    activity = activity_factory()
    admin_client.login(admin_user)

    rv = admin_client.get(f"/activities/{activity.id}/edit/")
    assert rv.status_code == 200


@pytest.mark.parametrize("direct_payment", [True, False])
@pytest.mark.parametrize("activity_price", ["not free", "0", ""])
def test_load_activity_with_form(
    direct_payment,
    activity_price,
    activity_factory,
    db_session,
    admin_client,
):
    activity: Activity = activity_factory()
    activity.price = activity_price
    db_session.commit()

    rv = admin_client.get(f"/activities/{activity.id}")
    assert rv.status_code == 200


def test_activity_picture(
    admin_client: CustomClient, activity_picture, admin_user, activity
):
    admin_client.login(admin_user)
    rv = admin_client.get(f"/activities/{activity.id}/picture/")
    assert rv.status_code == 200

    with hashfs.open(activity_picture.hash) as f:
        assert rv.data == f.read()

    rv = admin_client.get(f"/activities/{activity.id}/picture/thumbnail/")
    assert rv.status_code == 200
