import json
from urllib.parse import parse_qs, urlparse

from app.models.oauth.client import OAuthClient


def test_oauth_authorize_authorization_code(
    anonymous_client, member_user, oauth_client: OAuthClient
):
    anonymous_client.login(member_user)

    rv = anonymous_client.get(
        "/oauth/authorize",
        query_string={
            "response_type": "code",
            "client_id": oauth_client.client_id,
            "redirect_uri": oauth_client.redirect_uris[0],
            "scope": " ".join(oauth_client.scopes),
            "state": "somerandomstate",
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
    )
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/oauth/authorize",
        data={
            "client_id": oauth_client.client_id,
            "scope": " ".join(oauth_client.scopes),
            "response_type": "code",
            "redirect_uri": oauth_client.redirect_uris[0],
            "state": "somerandomstate",
            "confirm": "Accept",
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
        follow_redirects=False,
    )
    assert rv.status_code == 302
    location = urlparse(rv.location)
    querystring = parse_qs(location.query)
    assert "code" in querystring
    assert querystring["state"] == ["somerandomstate"]

    rv = anonymous_client.post(
        "/oauth/token",
        data={
            "code": querystring["code"],
            "client_id": oauth_client.client_id,
            "client_secret": oauth_client.client_secret,
            "redirect_uri": oauth_client.redirect_uris[0],
            "grant_type": "authorization_code",
            "scope": " ".join(oauth_client.scopes),
        },
        # Add specific HTTPS, as AUTHLIB_INSECURE_TRANSPORT is not set during test
        url_scheme="https",
    )
    assert rv.status_code == 200, rv.data
    data = json.loads(rv.data)
    assert "access_token" in data
