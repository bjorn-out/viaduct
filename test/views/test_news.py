def test_create_edit(anonymous_client, admin_user, news_factory):
    news_item = news_factory()
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/news/create/")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/news/{news_item.id}/edit/")
    assert rv.status_code == 200
