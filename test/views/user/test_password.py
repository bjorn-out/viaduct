from time import sleep

from flask_wtf.csrf import generate_csrf

from conftest import CustomClient


def test_member_change_password_self(
    anonymous_client: CustomClient, member_user, password
):
    anonymous_client.login(member_user)

    rv = anonymous_client.get("/users/self/password/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/users/self/password/",
        data={
            "current_password": password["raw"],
            "password": "new_password",
            "password_repeat": "new_password",
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200
    assert b"successfully" in rv.data

    rv = anonymous_client.get("/sign-out/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/sign-in/",
        data={
            "email": member_user.email,
            # Raw password is only set in the pytest fixtures.
            "password": password["raw"],
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200
    assert b"password you entered appears to be incorrect" in rv.data

    # We have to sleep at least 500ms, because we have rate limiting on login.
    sleep(1.0)

    rv = anonymous_client.post(
        "/sign-in/",
        data={
            "email": member_user.email,
            "password": "new_password",
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200
    assert b"password you entered appears to be incorrect" not in rv.data
