import pytest

urls = [
    "/groups/{group_id}/edit",
    "/groups/{group_id}/users/",
    "/groups/{group_id}/roles/",
]


@pytest.mark.parametrize("url", urls)
def test_vue_group_pages(url, anonymous_client, admin_user, admin_group):
    anonymous_client.login(admin_user)

    anonymous_client.login(admin_user)
    rv = anonymous_client.get(url.format(group_id=admin_group.id))
    print(rv.data)
    assert rv.status_code == 200
