from flask_wtf.csrf import generate_csrf

from app.models.user import User
from conftest import CustomClient

UPDATE_PROFILE_BODY = dict(
    first_name="new_first_name",
    last_name="new_last_name",
    address="new_address",
    zip="new_zip",
    city="new_city",
    country="new_country",
    birth_date="1995-01-01",
    study_start="2013-09-01",
    phone_nr="0123456789",
    locale="en",
    alumnus=False,
)


def test_user_profile(anonymous_client: CustomClient, member_user):
    anonymous_client.login(member_user)
    rv = anonymous_client.get("/users/self/view")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/users/{member_user.id}/view")
    assert rv.status_code == 403


def test_admin_user_profile(anonymous_client: CustomClient, admin_user):
    anonymous_client.login(admin_user)
    rv = anonymous_client.get("/users/self/view")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/users/{admin_user.id}/view")
    assert rv.status_code == 200


def test_edit_user_profile(anonymous_client: CustomClient, member_user: User):
    anonymous_client.login(member_user)
    rv = anonymous_client.get("/users/self/edit/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/users/self/edit/",
        data=dict(
            email=member_user.email,
            **UPDATE_PROFILE_BODY,
            csrf_token=generate_csrf(),
        ),
    )
    assert b"Profile succesfully updated" in rv.data


def test_edit_admin_user_profile(
    anonymous_client: CustomClient, member_user, admin_user
):
    anonymous_client.login(admin_user)
    rv = anonymous_client.get(f"/users/{member_user.id}/view/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        f"/users/{member_user.id}/edit/",
        data=dict(
            email=member_user.email,
            **UPDATE_PROFILE_BODY,
            csrf_token=generate_csrf(),
        ),
    )

    assert b"Profile succesfully updated" in rv.data
    rv = anonymous_client.get("/users/self/edit/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/users/self/edit/",
        data=dict(
            email=admin_user.email,
            **UPDATE_PROFILE_BODY,
            csrf_token=generate_csrf(),
        ),
    )
    assert b"Profile succesfully updated" in rv.data
