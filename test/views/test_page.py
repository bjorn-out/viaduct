from typing import Tuple

from app.models.page import Page, PageRevision


def test_page_404(admin_client):
    path = "/pagepathwhichisnotyetcreated"
    rv = admin_client.get(path)
    assert rv.status_code == 404
    assert "text/html" in rv.content_type


def test_page(admin_client, page_revision: Tuple[Page, PageRevision]):
    page, revision = page_revision
    # Page has now been created
    rv = admin_client.get(page.path)
    assert rv.status_code == 200


def test_create_edit(
    anonymous_client, admin_user, page_revision: Tuple[Page, PageRevision]
):
    page, _ = page_revision
    anonymous_client.login(admin_user)

    rv = anonymous_client.get("/pages/create/")
    assert rv.status_code == 200

    rv = anonymous_client.get(f"/pages/{page.id}/edit/")
    assert rv.status_code == 200
