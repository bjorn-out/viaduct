"""Remove category tables.

Revision ID: bb5f3c25eba7
Revises: cd87ed759c40
Create Date: 2019-10-23 08:10:31.906708

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.engine.reflection import Inspector

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "bb5f3c25eba7"
down_revision = "cd87ed759c40"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    conn = op.get_bind()
    inspector = Inspector.from_engine(conn)
    tables = inspector.get_table_names()

    if "category" in tables:
        db.session.execute('DROP TABLE "category" CASCADE ')

    if "category_category" in tables:
        db.session.execute('DROP TABLE "category_category" CASCADE ')

    if "category_page" in tables:
        db.session.execute('DROP TABLE "category_page" CASCADE ')


def downgrade():
    create_session()

    # Tables have never been filled, so do not create them.
    pass


# vim: ft=python
