"""empty message.

Revision ID: e63d2912523d
Revises: ('6ced54df5983', '11b500a5685d')
Create Date: 2019-12-14 10:02:38.325630

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "e63d2912523d"
down_revision = ("6ced54df5983", "11b500a5685d")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    pass


def downgrade():
    create_session()

    pass


# vim: ft=python
