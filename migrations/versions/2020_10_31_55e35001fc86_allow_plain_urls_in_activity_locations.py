"""Allow plain URLs in activity locations.

Revision ID: 55e35001fc86
Revises: 558a94f0560b
Create Date: 2020-10-31 16:17:28.903006

"""
from urllib.parse import unquote, quote

from alembic import op
import sqlalchemy as sa


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

# revision identifiers, used by Alembic.
revision = "55e35001fc86"
down_revision = "558a94f0560b"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


class Activity(db.Model):
    __tablename__ = "activity"
    id = db.Column(db.Integer, primary_key=True)
    location = db.Column(db.String(1024), nullable=False)


def upgrade():
    create_session()

    op.alter_column(
        "activity",
        "location",
        existing_type=sa.VARCHAR(length=128),
        type_=sa.String(length=1024),
        existing_nullable=False,
    )

    for activity in db.session.query(Activity):
        activity.location = "https://maps.google.com/?q=" + quote(activity.location)

    db.session.commit()


def downgrade():
    create_session()

    for activity in db.session.query(Activity):
        if activity.location.startswith("https://maps.google.com/?q="):
            activity.location = unquote(
                activity.location[len("https://maps.google.com/?q=") :]
            )

    db.session.commit()

    op.alter_column(
        "activity",
        "location",
        existing_type=sa.String(length=1024),
        type_=sa.VARCHAR(length=128),
        existing_nullable=False,
    )


# vim: ft=python
